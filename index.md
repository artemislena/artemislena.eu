---
title: "Artemis' Blog"
layout: front.liquid
---
<ol id="posts-listing">
{% for post in collections.posts.pages limit:15 %}
<li><article lang="{{post.data.lang}}">

### [{{post.title}}]({{post.permalink}})
</article></li>
{% endfor %}</ol>

[Older posts](https://artemislena.eu/posts/)
