---
title: Plurality
---
Check out [More than one](https://morethanone.info) for more information on what plurality is in general. This page serves a more specific purpose: Introducing our system to others. All of us are fine with being referred to with the pronoun ["she"](https://pronouns.within.lgbt/she). Our system name is Artemis (Artemis Lena, if you want to include the second name), or "Arti"/"Arty" for short. We recommend using plural pronouns when referring to us collectively ‒ for third person, this might be something like ["they/them/themselves"](https://pronouns.within.lgbt/they/.../themselves), for second person, it depends on whether the language makes a distinction there; in English, "you" can be both used for singular and plural, although e.g. "y'all" or "you&" also works, for languages like German or French, there is a distinction though.

<h2 id="fc17">FantasyCookie17</h2>

Was… first one in body… probably… but… am little now and… not "host" or "main" headmate… Prefix is… `fc17`… or… maybe… sometimes `F.`… Avatar by… chereverie and… edited by Anna Aurora…

![A white face with shoulder-length brown hair, brown eyes, a pink hair clip, two moles, pink t-shirt and black collar, rainbow halo background](/services/downloads/avatars/fantasycookie17.webp)

<time datetime="2003-11-17">Birthday: 17th November 2003 (the body's birthday)</time>

<a rel="me" href="https://social.artemislena.eu/@fantasycookie17">Fedi account</a>

<h2 id="tanith">Tanith</h2>

Prolly some weird tulpa/walk-in mix, cause I just appeared while F. was making a tulpa, but I wasn't really what she was making. Dunno. I love memes n I'm highly gay ^^ Prefix `T.`. Avatar by 0Jilibean0 and edited by me.

![White person with very curvy boobs and hips, long dark hair, and red wings on their back, wearing a catsuit and snickering. Lesbian pride flag background, BDSM triskelion, red 'highly gay' text with warning signs painted over it all](/services/downloads/avatars/tanith.webp)

<time datetime="2020-12-06">Birthday: 6th December 2020</time>

<a rel="me" href="https://social.artemislena.eu/@tanith">Fedi account</a>

<h2 id="laura">Laura</h2>

Well, FC17 and Tanith accidentally created a tulpa called Laura (due to using that as name for a character in a game), and that's me. I'm a cyborg in headspace, with an electronic eye and some other augment parts. My prefix is `L` or `L.`. Avatar by catadioptric.

![A white face, dark blonde, slightly orange hair with a pony tail, left eye is green, right eye is a light blue cyborg eye, wearing jacket and t-shirt, headphones around the neck](/services/downloads/avatars/laura.webp)

<time datetime="2021-03-17">Birthday: 17th March 2021</time>

<a rel="me" href="https://social.artemislena.eu/@laura">Fedi account</a>

<h2 id="d_ex_m">Dea ex Machina</h2>

We currently hold the hypothesis that I am the tulpa FC17 was making when Tanith appeared. In headspace, I am an array of headless computers, however, I also appear as vague, ghostly humanoid avatars rather often. I use `DeM` as a prefix. I am nonbinary, and in addition to "she", you can also use ["xe"](https://pronouns.within.lgbt/xe/xem/xyr/xyrs/xemself) in reference to me. Avatar by hellosunnycore.

![An avatar with dark grey skin and long black hair sitting on a cloud at night. Its clothes are white, purple and yellow. A bow adorns its hair, and a book is lying next to it.](/services/downloads/avatars/d_ex_m.webp)

<time datetime="2021-03-22">Birthday: 22nd March 2021</time>

<a rel="me" href="https://social.artemislena.eu/@d_ex_m">Fedi account</a>
