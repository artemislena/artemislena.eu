# artemislena.eu
This is our personal website, available on <https://artemislena.eu>.

## Be crime do gay webring
You identify as queer, have a personal website, and want to join a nice webring? Just use the JavaScript example from the example implementations below. Replace the link at the top of the script with the URL of your webring page, and put something like `<noscript>For making these webring links usable, JavaScript is <i>required</i>.</noscript><a id="webring_prev">Left</a><a id="webring_next">Right</a>` on an appropriate spot on the page, and contact at least one webring member. Be sure to mention your MXID so you can be invited to the invite-only webring room on [Matrix](https://matrix.org).

Want to implement things yourself? Have a look at [this JSON file](https://codeberg.org/artemislena/artemislena.eu/src/branch/main/_data/webrings/beCrimeDoGay.json), also available [here](https://artemislena.eu/services/downloads/beCrimeDoGay.json). You need to parse it via a JavaScript, server-side code or static site generator in order to generate links on your webring page. Example implementations are linked below. If you need help, [contact us](https://artemislena.eu/contact.html). Once you have done that, submit a pull request changing that JSON file, or open an issue here with your webring page's URL. Alternatively, contact a webring member.

### Example implementations for generating the links
* [JavaScript](https://codeberg.org/annaaurora/annaaurora.eu/src/branch/main/static/webrings/be-crime-do-gay-webring/nav.js)
* [Liquid (Cobalt)](https://codeberg.org/artemislena/artemislena.eu/src/branch/main/_includes/becrimedogay.liquid)
* [Tera (Zola)](https://codeberg.org/annaaurora/annaaurora.eu/src/branch/be-crime-do-gay-webring-tera/templates/webrings/be-crime-do-gay-webring.html)
* [Apache SSI](https://git.sr.ht/~ecc/ellie.clifford.lol/tree/master/item/http/md/index.md?view-source#L41) with [Python CGI script](https://git.sr.ht/~ecc/ellie.clifford.lol/tree/master/item/http/static/cgi-bin/webrings/be_crime_do_gay.py)
