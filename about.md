---
title: "About"
---
## Who are you?
See [this page](/plurality.html).

## I cannot access your website!
There might be several reasons for this:
1. Our IP address has changed. Wait a few minutes until the DNS record is refreshed.
1. Our router or server are currently rebooting to apply updates. Wait a few minutes.
1. We messed something up, or our hardware is malfunctioning. We are likely already aware, and hopefully it will not take long before things are accessible again.
1. Your ISP, government, or another entity that can mess with your network have blocked this domain or IP address. Try using [Tor](https://torproject.org) or a VPN.

## Can I share your content? What license do you use?
The source code (CSS, templates and such) is licensed under the [Unlicense](https://opensource.org/license/unlicense/). All *content* that was written by us is available under [CC-BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/legalcode), unless otherwise noted. Content that is quoted
> like this

or <q>like this</q> is typically licensed differently (a link to the source with additional information will normally be provided) and used under "fair use" laws. Content available on external sites we may link to is licensed independently of content on this site.

Some content linked to under [`/services`](/services/) might be licensed differently, the corresponding repo with the license always being linked in such cases. Images shown on [the Webrings page](/webring.html) are used with permission and may be licensed differently, see the individual webring sites for further information. Avatars on [the plurality page](/plurality.html) are used with permission or according to their licenses, belong to their respective artists and are probably not available under any Creative Commons license.

## How can I make sure I don't miss any of your posts?
Subscribe to [the RSS feed](/{{ collections.posts.rss }}) or [the JSON feed](/feed.json). Typically, you will need a special software or browser extension to do so. You can also [join our Matrix room](/contact.html#matrix) or [follow our Fediverse (Mastodon) accounts](/contact.html#fedi). The latter two can also be used for commenting on our posts.

## Do you have any specific policies, other than your [privacy policy](/privacy.html)?
- Links to our posts will never [rot](https://en.wikipedia.org/wiki/Link_rot), unless we switch to a different domain, and even then, we will try to redirect the old one to the new one as long as it is viable for us to keep it. [This site is designed to last.](https://jeffhuang.com/designed_to_last/)
- No JavaScript required for the blog and any services that do not need it.
- Our posts should be readable on any kind of device that is suitable for browsing the internet, and nothing on the site should look off, no matter the (reasonable) font.
- Edits: Edits on the meta pages, as well as to the styles, will be done without notice. Edits to posts follow the following policy:
1. If the edit simply fixes a typo, it will be done without notice.
1. If the edit is an update that reinforces the post's opinion, it will be added to the end of the relevant paragraph or post, together with a notice.
1. If the edit is an update that contradicts the post's opinion, it will be added to the beginning of the relevant paragraph or post, together with a notice.
1. Sometimes we may deem it to be appropriate to create an entire new post for updates.
- We will never compromise the privacy of visitors by tracking them or similar.
- We try to keep this site as secure as possible.
- This site uses standard HTML5 with semantic elements and so on. It should be as accessible as possible.

Should you find any of these policies to not be followed, [contact us](/contact.html).
