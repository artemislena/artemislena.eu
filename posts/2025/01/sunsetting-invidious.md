---
title: Sunsetting Invidious
published_date: 2025-01-11 15:58:20 +0100
data: {author: Tanith}
---
So, as ya might've known, our Invidious instance was down for a long time; couldn't get't connecting w the DB anymore. We solved't by running't outside a container which somehow made that work, but anyway, 't feels like we can't really run that software well anymore. Additionally, Youtube's been making't harder n harder circumventing their rate limits, n honestly we can't keep up w that, especially when the Invidious patches take ages'a upstream so we'd needa apply them manually somehow.

Thus, we're shutting down the instance on June 25th this year (we made the decision on December 25th last year n wanteda give a six months grace period). Don't expect video playback working on there at all, it's just so ya can move off ur data from there'f ya got an account on there. After that date, all user data'll be deleted irreversibly, so make sure ya don't miss't'f ya got any important subscriptions ya don't wanna forget bout or whatever. 'f ya need a reminder, the instance's on
<a href="https://yt.artemislena.eu" rel="nofollow">yt.artemislena.eu</a> (note: after the mentioned date, that link won't work anymore).
