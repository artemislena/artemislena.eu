---
title: Fragments 4
data: {author: Laura}
published_date: 2025-01-11 16:04:10 +0100
---
Oh my, it's uh, been a bit over 3 years since we [last did a Fragments post](/posts/2021/12/fragments3.md). Anyway, here's two more cute/wholesome/fun snippets from our internal quadrilogue.

> fc17: The snuggling squad… they… come at night and then… they snuggle you!  
L: Oh no! Do you think I'm prepared? *extends the zappers on her forearms*  
fc17: Those probably… won't help… They're… professional snugglers…  
L: Hm, are you a professional snuggler?  
fc17: No…  
L: Am I a professional snuggler?  
fc17: No… unless… do you have a snuggling license?  
L: *checks pockets* Hm, no, don't seem to have one with me.  
fc17: Then… you can't… call yourself… a professional snuggler…  
L: But can I still be a snuggler?  
fc17: You are a snuggler! *snuggles tighter into Laura's embrace*

> DeM: It is always a pleasure cuddling with you, milady.  
T.: Oh, I'm a "m'lady" now? ^^  
fc17 (to Laura): Are they… flirtiiiing?
