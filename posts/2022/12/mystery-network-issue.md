---
title: Mystery network issue
data: {author: Tanith}
published_date: 2022-12-23 23:53:48 +0100
---

Were ya clickbaited? Did ya open this post cause me adding "mystery" into the title tricked ya into thinking this post'd be interesting? ^^ Well, either way, too late, ur now legally n morally obligateda read this post :P Who knows, maybe ya think it's interesting after all, yk. Also, we make 0 money or whatever from this, lmao.

Basically, stuff went like this:

Maybe a couple weeks or so ago, a couplea our services went down, specifically all those running in a Podman container, which's all those on our Zotac Zbox Ci645 nano (just gonna call't Ci645, that's shorter). That's Invidious, CryptPad, Teddit, Libreddit, BreezeWiki, Nitter, ProxiTok n Ntfy. We were at uni at the time, so couldn't look into't immediately, as the machine in question wasn't reachable over the network, either. So, once we could physically access't, we checked what was going on, n't turned out the machine was running as expected, except for the fact't couldn't build any network connectiona the outside world. Rebooted't, n stuff went backa normal, couldn't reproduce the issue.

Yet, a few days later, the issue came back. Ofc, we'd wanna find out'f there was some easier waya fix't. Turns out there is ‒ just run `ip link set enp44s0 down`, then `ip link set enp44s0 up`. Strange, huh? The issue occurred two more times, during which we found out even just unplugging n replugging the cable'd work, tho, 't clearly ain't a cable issue, seeing that soft resetting the interface works without even touching the cable. Anyway, we had no idea whatta do anymore, so as a band-aid, we set up a timer that runs those `ip link` commands directly after each other once an hour. Ideally, that'll prevent this from happening in the first place (i.e. maybe't only happens'f the interface stays up for several hours at once), but even'f not, at the very least, it'll never last for more than an hour, even'f we're not at home, or take a whilea notice.

So, wrapping things up. We got the Ci645 not only cause it'd've better performance n more expansibility ('s that a word?) than most ARM machines, but also cause we were hoping we wouldn't get driver/bootloader bugs n what not we'd been getting on ARM SBCs on the oh-so-much better supported x86-64 platform. Heh. Apparently not. Then again, t'least the platform firmware (UEFI) overall works fine. Anyway, anyone know what might be going on here? Hearda weird driver issues the kernel might be'ving w certain NICs, or anything like that? Feel freea let us know ^^ Cya next time. N like, hope ur'ving nice holidays, 'f ur'ving holidays rn <3 (or just like, yk, a nice time in general)

Wait, ya actually read this post until the end? :P
