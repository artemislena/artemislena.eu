---
title: "Webring and new theme"
data: {author: "Tanith"}
published_date: 2022-05-10 21:23:35 +0200
---
Some friend suggested we made a webring called "Be crime do gay", n guess what. That's what we did ;D Ya can join too, just contact us (n ideally another ring member). More info [here](/webring.html).

In other news, we changed our theme n stuff quite a lot. Fixed a few things, made the light theme a bit better, made the dark theme so much better, n did the same over on the GTRR too. Now there's more pink n purple stuff ^^ Also we're using emojis as icons on the menu entries n for the archive (that was were we originally got the idea from as our SVG usage was causing some issues, especially on the dark theme). Idk, it's more fun n looks much nicer this way, I'd argue, n't takes less bandwidth than loading some SVG. Can't see the changes? Clear cache for the site.
