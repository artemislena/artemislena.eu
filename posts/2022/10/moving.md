---
title: Moving
data: {author: Tanith}
published_date: 2022-10-10 22:13:59 +0200
---

So, we found a flat! That's, well, great, Ig ^^ Now, we said there was gonna be some downtime. We still dunno *when*, tho. We're moving in on October 22nd, but't might very well be that our internet plan won't be valid before November 1st, in which case we'd fetch our servers from our current place on November 5th or 6th, cause we don't wanna run em on the internet connection they got there currently. So, there's gonna be some downtime, it's gonna be several hours, n it's gonna be on a weekend, either the last two October weeks, or the first November week. Again, gonna update once we know more.

Now, what we're getting's Vodafone CableMax 1000, which seemsa be the best thing we *can* get at the new place, n at 1000Mb/s down / 50Mb/s up, it *is* an upgrade over what we got currently. However, it's also gonna've CGNAT, *afaik*, so we're gonna'vea tunnel IPv4 via a VPS (likely the cheapest we can get from Netcup), meaning'f ya can, do prefer IPv6 by then, plz (otoh, our IPv4 address'd be static then, not that I think dynamic DNS causes *that* much downtime).

<ins>Not taking new applicants anymore. Already decided for somebody.</ins>

## Looking for a flatmate
Yeah, [yet again](/posts/2022/07/looking-for-a-flatshare.html). I really don't wanna turn this into a blog on ads, but like, we found one flatmate already, but our new flat got three bedrooms, n the places we're planninga share this ad in're a bit impractical for longer texts, I feel, so it's easier just linkinga this post. So, don't feel obligateda sharing this elsewhere'f ya don't know someone w an urgent need for a new living space ‒ we expect we're gonna find somebody in a group we're in even'f ya don't.

### The flat
It's located in southern Berlin (more precise location only available for applicants), got, as mentioned, three (rather large) bathrooms, as well as a small kitchen n bathroom. Ur room'd be the medium-sized one; around 21.6 square meters, windows directly facing the street (it's a ground floor flat), n no furniture yet (presumably, we still partially gotta figure out what the previous tenants're gonna leave behind.

### Ur flatmates
That'd be [us four](/plurality.html), as well as another trans girl ‒ who doesn't speak German yet, so ya would needa be ablea speak English.

L: Or uhm, both Latvian and German? That'd uh, be impractical, though, probably.

T.: Or that, yeah ^^

### Whom we're looking for
Should go without saying, but no one who's fascist/nazi, homo/transphobic, or otherwise hateful or discriminatory. WLINTA (Woman/Lesbian/Inter/Trans/Agender) people strongly preferred. Plurals, neurodivergents n queers highly welcome ^^

fc17: Cuddly people, too…

Now, we don't want a pure "utility flatshare". We'd like being friends, chosen family, or sth along those lines w the people we live together w. We don't want *obligations*, but some willingnessa do things together ‒ yk, having meals, cooking, baking, playing, watching a film or what not ‒ *at times* (i.e. whenever ya got spoons n time) 'd be appreciated. As for sharing meals, I'd s'pose things like basic food should be shared among habitants, including the costs for them (as far as people got the financial means for that). Also brings usa the fact we're mostly vegan, n we'd appreciate'f any shared meals're vegan, n there's no non-vegetarian food in the flat. Chores (like cleaning, groceries, as for the latter, simply shifting between who does that regularly might be enough for distributing shared food costs) should be distributed among flatmates, we'd be happya work out some plan/system for that, in case we don't manage a fair distribution without that.

### Boring stuff ‒ money n legal things
Ya could likely move in by November 1st, perhaps a couple days earlier. For ur room, we expect a 460€ rent. Note this doesn't include GEZ fees or electricity costs ‒ those'll be shared evenly among flat members. 't does include internet n heating costs, however. Legally, ya'd be subletting, but since the landlord/flat management should allow this, registration ("Anmeldung") as a Berlin citizen should be possible. Initially, the contract'd be limiteda sth like three months or so, but that's just a safeguard in case living together doesn't work out at all, 'f't does (which I sure hope't will), we'd give ya a long-term contract.

DeM: We are relatively certain this is a standard procedure, and in any case we would make sure not to make you homeless suddenly after those three months are over, i.e. we would want to make sure you found another place to live, before we could terminate the contract without hurting our conscience.
