---
title: Downtime announcement
data: {author: Tanith}
published_date: 2022-10-26 18:08:59 +0200
---

This's gonna be a short one ^^ As promised, we'd tell ya when we know when exactly there's gonna be a long, planned downtime. Turns out, the internet connection at our new place could already be activated, so we're taking our servers w us this Saturday. Expect downtime between 15:00 and 23:00 (all times CET), hopefully a bit less than that, n'f things don't go as planned (unexpected network issues, train delays) more than that.

Now, we verified we're behind a CGNAT, n finished writing configs for an IPv4 tunnel VPS ‒ this means IPv4 connectionsa our servers're gonna've extra delay n cause extra load on our servers, tho t'least it's gonna be static (unlike IPv6, which's direct n dynamic). Also, please don't send more than 80TB per month on IPv4 (cause then our VPS provider's gonna throttle our connection) ;)
