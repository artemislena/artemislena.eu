---
title: September 2022 thoughts on administration
data: { author: Tanith }
published_date: 2022-09-11 00:05:41 +0200
---

I didn't wanna've yet another post titled "Update", went thru various possible titles, n well, here we go.

## Proxy changes
We set up a [ProxiTok](https://github.com/pablouser1/proxitok) instance, so ya can privately watch TikTok videos. Want other proxies? We might be willinga host anything supported by [LibRedirect](https://libredirect.github.io/), as long as it's easya host w automatic upgrades on NixOS, n does replace a service that's actually bad (we don't consider Wikipedia "bad", so we won't host WikiLess).

[reddit.artemislena.eu](https://reddit.artemislena.eu) (as well as its `www` subdomain) now redirectsa [teddit.artemislena.eu](https://teddit.artemislena.eu), cause it's a Teddit instance now ‒ saves bandwidth as't got caching, n supports RSS feeds. Also should support `i.redd.it` links (rewritten by the UntrackMe Android app), [theoretically](https://codeberg.org/teddit/teddit/issues/336). Furthermore, doesn't need JS for video playback w audio. Now, Libreddit does do the UI better, so we might let't return under `lr.artemislena.eu`, provided [this issue](https://github.com/spikecodes/libreddit/issues/585)'s fixed.

Our Invidious instances now restart twice rather than once an hour, so that DB error we keep getting'll hopefully happen less often.

## Onion domains
Somea the sites/services we host're now available under Onion domains (not all, cause w some it's either not possible or not sensible). See [the services page](/services/) n [the verify page](/services/verify.html) for links.

## New hardware
We got a Zotac Zbox CI645 Nano. Could go into detail here, but that might just bore ya? Stuff got moved around.

L: And we could remove two machines from our setup, while only adding a single new one. Oh, also, our Borg server runs on SSDs now. Main reason being, uh, they don't make any noise, which is likely important when we move.

## Future stuff
Moving's a good keyword. We're likely gonna move sometime later this month or at the starta October. That means there's gonna be some downtime, quite possibly for several *hours*, as carrying the servers from one placea another does take time. N then we gotta make sure the LAN at the new place's set up correctly, n what not. We'll try keeping ya up to date; once we know when there's gonna be a downtime, we'll announce't in another post.

We're considering setting up Certificate Transparency (CT) monitoring in some way. In case ya don't know, CT's a logging system for TLS certificates, and non-logged certs won't be accepted by Chromium or Safari. We'll almost certainly wait for the Caddy 2.6.0 release, as [that'll implement some events thingy when certs're issued](https://github.com/caddyserver/caddy/pull/4984). Now, originally I wanteda say "stay tuned" here, but, well, note I said *considering*, not *planning*. We looked into stuff further n not sure whether we wanna do't. Firsta all, it's hard. [ProtonMail's CT monitoring tool's unmaintained n broken](https://github.com/ProtonMail/ct-monitor/issues/2), Google's own CT tools we couldn't find proper documentation on, n the only public API sorta thing we could find was the [crt.sh](https://crt.sh) one, which only offers that via RSS, which, being XML-based, [may've cause security issues in Python](https://docs.python.org/3/library/xml.html). Then again, perhaps not? From that page:

> Since Python 3.7.1, external general entities are no longer processed by default.

Dunno. Still seems scary, but we also don't feel confident doing things in a different language than Python (parsing things in shell scripts'd be a pain, Rust we just don't like anymore n feel we don't understand't well, n we don't know any other memory-safe language). Secondly, what's the point in doing this even? The point's distrusting the way current web PKI's done (i.e. via DNS(SEC) n CAs). But, 'f the system in general's compromised, we got other issues, like our applications themselves perhaps being malicious, as they're usually fetched from HTTPS sites, w not much other verification done by the package maintainers. 'f a large-scale attack happens, well, others gonna notice, won't they? They got their own monitoring, *hopefully*. N in casea a small-scale, targeted attack, well, we're not valuable enough for being targeted, 're we? By adding those onion domains, we already offer a method for fixing somea the issues w web PKI (tho, not a particularly user-friendly one). Lastly there's also the thing that this's a thing we would'vea monitor manually ‒ 'f we already distrust the PKI system in some way, we can't trust our Ntfy server either, so we would'vea log in manually into the server which checks these things, n verify on there.

L: And uh, in the general system compromise case, we probably couldn't trust crt.sh either.

Yeah, that too. In the end, perhaps monitoring this stuff's rather lefta people who got the time for this? We do this as a hobby, not a full-time job. Bringing usa the last sectiona this post.

## General thoughts on administration
Migrating stuff onto the new hardware took *ages*.

L: We were much later in bed than planned. And yeah, it's late again today.

fc17: Mrew…

Yeah. Can't do that once we're at uni. As is, we already don't get enough sleep cause, lately, we wake up earlier than we should, but don't fall asleep again. I'm not sayin we *hate* doing server admin stuff as a hobby. It's just, yk, 't might become problematic at some point? What'f hardware dies? We would'vea order new hardware, n't might take several days arriving ‒ yk what that means for uptime. N then we would'vea do a time-consuming hardware migration again, n now let's say we're'ving an exam at uni the next day ‒ yeah, that won't work out really. We still *wanna* provide this stuff, we use't ourselves after all, n also don't wanna disappoint other people who use't often, perhaps even daily.

DeM: But it would be presumptuous to assume we can provide a reliable service like, say, large companies do usually, if our professional life is focused on entirely different things. We hope it is *enough*, but we cannot know for certain. Will people, for example, be able to rely on the [GTRR](https://gtrr.artemislena.eu), with that premise? We do not know, but we certainly do know that we ourselves tend to not try to reach a site again the next day if we visit it for the first time, and it appears unreachable.
