---
title: "New stuff"
data: {author: "Tanith"}
published_date: 2022-06-01 20:02:53 +0200
---
Huh, looks like I'm the only onea us writing posts here recently.

Anyway, ever caught urself thinking <q>Oh girl, I wish there was some new stuff hosted by artemislena.eu</q>? Well, I got good news for ya, cause we do've some new stuff. Just look over at [the services list](/services/), we got a Ntfy server n a Rimgo instance now. Even'f ur not interested in either, ur still gonna profit from't'f ya use our services, given thxa our Ntfy server, we now get push notifs when backups fail or succeed, so no [surprises](/posts/2022/01/major-data-loss.html) there again.

Besides that, we also got some CSS changes (CSS sucks, it's way too inconsistent across browsers, n surprisingly enough, Safari actually was the one which implemented what we did more or less best, I'd argue), so I hope ya enjoy the new theme. The text on the menu button might actually be centered now sometimes, on some browsers? Also the entries on our contact page now got icons.

In perhaps not so great news, we stopped hosting any game servers, but like, who used ours anyway? That's cause we migrated some thingsa different machines, so we could use binary sources closera the upstream (sometimes there's no official ARM container image available, so we useda use third party containers, but now we got a machine for AMD64 containers), which does mean more trustworthy, up-to-date code n some CryptPad improvements ('t doesn't break on Safari anymore, we don't gotta maintain the CSP manually now, n we increased the default storage on ur CryptDrive to 500MB, n we can give ya more'f needed).

Oh, n happy pride month, everyone 🏳️‍🌈
