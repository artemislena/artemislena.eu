---
title: "HSTS preload and Upptime"
data: {author: "FantasyCookie17"}
published_date: 2022-06-02 11:53:19 +0200
---
Tanith complained in the last post she was the only one writing things on our blog recently… so thought… I'd write something again…

T.: Aw, c'mon. I didn't *complain*. It's alr ^^

When we checked again today, we noticed [artemislena.eu is now part of the HSTS Preload list](https://hstspreload.org/?domain=artemislena.eu)… so, hopefully, on your next browser update… our domain's HSTS status will be preloaded into your browser,
meaning all our domains will be forced to connect via HTTPS without even a single possibly plain HTTP connection needed… If you have your own domain… maybe you want to have it preloaded too? The requirements are specified on the website…

We also set up [Upptime](https://upptime.js.org) now… It's an uptime and response time monitoring service that works via GitHub… You can view our instance [here](https://upptime.artemislena.eu)… Note it's hosted on GitHub's servers, not our own…
