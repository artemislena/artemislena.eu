---
title: Christopher Street Day Rostock 2022
data: {author: "Tanith"}
published_date: 2022-07-16 22:07:48 +0200
---

Our last post was a pretty gloomy one overall (or t'least, twas for us, n we're still looking for flatmates), but I promise this one's just good vibes ^^ So, we were at Christopher Street Day in Rostock today, n twas a lotta fun. (As for what that's, several cities here in Germany call their pride parades "Christopher Street Day", or shortly "CSD", in remembrancea the Stonewall riots happening in Christopher Street back in 1969, note it's not a fixed date (in which case that'd likely be June 28), but different cities pick different weekend days in June n July, n Rostock happeneda pick July 16 this year).

Anyway, mostly just wanna share some pics w y'all ^^

![Us, wearing pink jacket, a pink FFP2 mask, and a transgender flag as a cape, held together by a lesbian flag pin, holding a waving progress flag (with a polyamorous flag on the other side, not visible in the picture)](./outfit.webp)
![Us, wearing the same outfit, standing on some sort of crate or so, waving our flag, polyamorous flag being better visible this time](./standing.webp)

So, that's us :) Ik, our facial expression doesn't show't, but we did've lotsa fun ;D Ya might've noted our polyamorous flag (it's on the same pole as the progress flag) on the pics, it's fun how like tonsa people (t'least 6 or so) asked us what't meant.

![Lots of rainbow people (i.e. people wearing and carrying various colourful pride flags and the like)](./view1.webp)
![Rainbow people walking along a curve](./curve.webp)
![Rainbow people standing on a square](./view2.webp)
![Rainbow people walking towards some body of water, presumably the harbour area](./view3.webp)

Talkinga lotsa people, there really were a lotta them. Estimates said around 5000. Note these pics're chronologically ordered ('f that matters at all), also, generally we were somewhere in the middle, so overall there was a similar amounta people behind us as well. Yeah, twas **huge**. Ya might notice someone having written <q>hug me</q> on their pride flag, regarding that, there really were quite a few offering <q>free hugs</q> n the like, which's an offering we gladly took, in general ^^ Kinda sad we didn't offer the same, but we're prepared for next time we're at a pride event:

![A progress flag with "FREE HUGS" written on in black on the yellow stripe](./freehugs.webp)

As for slogans on posters n such, there also were quite a few others that made us smile, besides the one w hugs. E.g. (translated from the original German texts) <q>watch more hentai</q> or <q>lesbians love trans women</q>. Oh, n before I forget't, *kink does belong at pride*. The puppyplay scene was quite present, n I think that's pretty cool, n I'd s'pose some other outfits were somewhat kinky in some ways too. I mean, kink's a divergent kinda sexuality (as's, say, homosexuality) (or really, kink can just be a lifestyle thing, it's not even inherently sexual for everyone), n pride parades're onea the few events/places where they can just be themselves n proudly show't off. Also don't forget how historically, kinksters played quite a large role in queer liberation n pride, so even'f ya don't see kink as an aspecta queerness, the kinky n queer communities're definitely connected in some way.

N thus concludes this post. We were admittedly scareda too much noise, but ngl, given our good mood, twas generally fine as long as we didn't get too closea the music truck whenever they turned up the volume. So'f that (or anything else) 's keeping ya from goinga some pride event, I urge ya, do try't out anyway. Twas such a good experience overall, seriously. Pride month's over, but stay queer anyway, y'all who're queer, n keep being proud ;P
