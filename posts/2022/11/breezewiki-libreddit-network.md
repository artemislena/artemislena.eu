---
title: BreezeWiki, Libreddit, and network changes
data: {author: Laura}
published_date: 2022-11-25 20:01:48 +0100
---

## BreezeWiki
[Cadence teasered BreezeWiki some time ago](https://cadence.moe/blog/2022-09-01-discontinuing-bibliogram), and since then we've uh, actually been quite excited to host it, and then [its release was announced](https://cadence.moe/blog/2022-11-06-introducing-breezewiki) some time later, and we've been hosting it pretty much since then. If you uh, ever need to visit a Fandom site, feel free to use our instance as a proxy for that. No more ads and weird video banners.

## Libreddit
We discontinued hosting Libreddit a while ago, but, since [it's maintained again now](https://github.com/libreddit/libreddit/issues/599#issuecomment-1297851595), we were glad to host it again. It uh, still doesn't have caching though, so uh, if you want to conserve our bandwidth, please use Teddit, or uh, if you need RSS feeds, or videos with audio that work without JS, Teddit would also be the service of choice.

## Network
Turns out Vodafone offers full Dual Stack (no DS-Lite, no CGNAT) for free, if you ask for it. So, that's what we did, and now we don't need to route IPv4 traffic over that VPS anymore ‒ if you previously configured your stuff to only connect to us over IPv6, well uh, that's not necessary anymore.

## Cuddler Webring
fc17: We… also joined… a new webring… it's cuddly…

Yeah, uhm, check it out on our [webrings page](/webring.html).
