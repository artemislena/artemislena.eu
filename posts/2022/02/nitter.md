---
title: "Nitter"
data: {author: "Laura"}
published_date: 2022-02-06 09:44:37 +0100
---
So uh, we set up Nitter, over at [tw.artemislena.eu](https://tw.artemislena.eu). Also, Matrix isn't currently working, we hope it will work soon after it has synced stuff, but can't guarantee anything. A lot of people tried to reach us over Jami instead, which didn't work, so we aren't officially reachable over that anymore, meaning currently, you should probably try either Signal or e-mail (or CryptPad, if you have to). Alternatively, [fc17 has an alt at fairdust.space](https://matrix.to/#/@fantasycookie17-alt:fairydust.space). And yeah, that's it.
