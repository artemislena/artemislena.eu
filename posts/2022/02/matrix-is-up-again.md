---
title: "Our Matrix server is up again"
data: {author: "Dea ex Machina"}
published_date: 2022-02-19 20:52:56 +0100
---
As you may have noticed, [our Matrix server has been down recently](/posts/2022/01/major-data-loss.html). Now, it has been up for the last few days, however, we were waiting a bit to publish this post, wanting to set up the bots first. We had some trouble installing NixOS on the corresponding machine, however, especially over the last days as [the `nixos-unstable` channel was blocked](https://github.com/NixOS/nixpkgs/issues/159612), forcing us to switch to `nixos-unstable-small`. Now, however, these are running too, [and our JSON Feed bot even received an update](https://codeberg.org/artemislena/jsonfeed-bot/commit/9ea66b6349f9d0732c9aa4d1449e8750b202f35f).

## What to do if you are a user on our server
Your old password will not work. We assume this is to us having previously configured the password pepper incorrectly. If you have not received a password reset yet, [contact us](/contact.html). Furthermore, some rooms you had previously joined will seem gone, if your date of joining was after the last backup was created. You can simply rejoin these, even if they are invite only, by using a `matrix.to` link with a `via` set to a server that is not `artemislena.eu`, but participates in the room. Lastly, I am afraid most, if not all of your old encrypted messages will end up being undecryptable.

## What to do if you are a user on another server
Normally, nothing. However, if a user on our server seems unresponsive in any room, try to open a new private chat with them, as it may well be they missed your messages.
