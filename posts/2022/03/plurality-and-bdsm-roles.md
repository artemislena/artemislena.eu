---
title: "Plurality and BDSM roles"
published_date: 2022-03-30 22:22:22 +0200
data: {author: "Tanith", cw: "kink"}
---
Turns out we had a post t'least once every month so far since December 2020 (n actually every month since we started back in Jul 2020 barring Nov 2020). N we kinda didn't wanna break that streak, n I did actually've a post in mind.

Before we getta the main topic, just a note that our Veloren server's up again, but we noticed we kinda lack interest in games without a good story/plot (while gameplay's secondary as long it's not as boring/grindy as in *Beholder 2*, I mean, we really love the Freebird Games games, n those're basically just story, not much gameplay at all), n seems like the recent Veloren updates didn't add much in that regard. For now, it's up, anyway, n'f ya wanna play w us, just ask, we might've time.

Might make a separate post on that too now I'm considering't, anyway, what I'm here for today's a post I published a while ago on FetLife, [over here](https://fetlife.com/users/14293511/posts/7937800); I basically just copied that n shortened/altered't a bit:

For context, overall, our system's a switch, tho that doesn't applya everyone, n, well, I'm a switch, but pretty dommy usually. So, recently, one headmate noted I'd kinda be "the ideal dom" for her, n I was like "Huh, wild. Y'd that be?"

N then I realised twas actually pretty simple. Like, when we assume our BDSM roles, we're usually tryna be a bit like we expect a partner would wanna usa be like. N ofc, ur headmates can influence quite a lot what ya expect a partner might want from ya.

What does that mean in effect? Dunno. Prolly not much, except that maybe partners who've similar desires or role expectations as my headmates might get better along w me than others. Or maybe the fact I've got a pretty subby person for giving me feedback might partially make up for my lacka IRL experience in some regards? ¯\\\_(ツ)\_/¯

Anyway, just thought this might be interestinga share. 'f ya comment over on Matrix (or email or however else ya reach us), n ur comment's interesting enough, I might ask for permission sharing't down below, just in case some other thoughts're relevant for this subject.
