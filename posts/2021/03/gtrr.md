---
title: "Global Transgender Resources Registry / Globales Transgenderressourcenregister (GTRR)"
published_date: 2021-03-29 05:50:07 +0200
data: {author: "FantasyCookie17"}
---

## English
Recently, my psychologist, after hearing about the amount of trouble I went through with my gender transition, especially in finding out whom to talk to in order to for medical and legal measures, suggested to me that I could try to start a sort of self-help group in order to help other people facing similar issues, which after all was how many queer support groups started existing. I honestly was not too intrigued by the idea at first, but then came up with something similar that sounded more fun: The Global Transgender Resources Registry. Basically, it is a collection of static webpages sorted by countries and regions and cities in those countries, listing various resources that might be helpful for transgender people seeking transition in the respective areas. This includes, but is not necessarily limited to:
* An overview of legal name and gender change procedures, if possible
* Lists of medical professionals and clinics helpful with medical transitioning ‒ psychologists, psychiatrists, endocrinologists, dermatologists, surgeons, etc.
* Lists of local support groups (both for transgender people specifically, or for queer people or minorities in general)

It is reachable [at gtrr.artemislena.eu](https://gtrr.artemislena.eu). Contributions to populate the registry can be done on [Codeberg](https://codeberg.org/artemislena/gtrr/), and are very much appreciated and needed. If you want to make
a contribution, but are unable to use Codeberg, [contact me](/contact.html).

<div lang="de">

## Deutsch
Vor kurzem schlug meine Psychologin, nachdem sie von meinen Problemen bezüglich meiner Gendertransition, insbesondere dabei, die richtigen Ansprechpersonen für medizinische und juristische Änderungen zu finden, hörte, vor, ich könnte eine Art Selbsthilfegruppe oder so gründen, um andere Personen mit ähnlichen Problemen zu unterstützen, schließlich waren auch andere queere Gruppen auf diese Weise entstanden. Ehrlich gesagt war ich von der Idee zunächst nicht allzu sehr angetan… Mir fiel dann aber etwas ähnliches ein, was mich mehr interessierte: Das Globale Transgenderressourcenregister (GTRR). Es handelt es sich um eine Sammlung an statischen Webseiten, sortiert nach Ländern sowie Regionen und Städten in diesen Ländern, welche verschiedene Ressourcen auflisten, die für transidente Personen in den jeweiligen Gebieten hilfreich sein können. Beispielsweise können folgende Dinge dort enthalten sein:
* Eine Übersicht an Gesetzen und Bestimmungen bezüglich Namen- und Personenstandsänderung, falls möglich
* Listen von Ärzt\*innen und Kliniken, die Hilfe bei der medizinischen Transition anbieten ‒ Psycholog\*innen, Psychiater\*innen, Endokrinolog\*innen, Dermatolog\*innen, Chirurg\*innen, etc.
* Listen von lokalen Unterstützungsgruppen ‒ sowohl spezifisch für transidente Personen, als auch allgemein für queere Personen und Minderheiten

Es kann [unter gtrr.artemislena.eu](https://gtrr.artemislena.eu) aufgerufen werden. Beiträge, um das Register mit Daten zu füllen, können auf [Codeberg](https://codeberg.org/artemislena/gtrr) getätigt werden, und werden dringend benötigt, ich freue mich also sehr über deinen Beitrag. Solltest du etwas beitragen wollen, aber nicht in der Lage sein, Codeberg zu benutzen, [kontaktiere mich](/contact.html).
</div>

<ins>Update: Comments on [Tildes](https://tildes.net/~lgbt/vyo/the_global_transgender_resources_registry)</ins>
