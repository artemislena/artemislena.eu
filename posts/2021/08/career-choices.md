---
title: "Career choices"
published_date: 2021-08-29 23:53:45 +0200
data: {author: "FantasyCookie17", cw: "Ramblings on career choices, some negativity"}
---

I have thought about this for a while now, and after collecting most of my considerations in a private chat with some other people, I realized I could as well make a blog post of this…

T.: *I* realized that. That one mustve been one of ur longest messages so far lmao, so I was like "dudette, why don't ya write a blog post?".

… fair. Anyway, our problem is that we used to be relatively certain we wanted to study computer science, however, recently we have been too much interested in psychology to entirely ignore that… I thought I would collect some of my thoughts on both subjects here, and then share it in other places besides the usual Bloggers Gathering and our own room. Of course, no one besides ourselves can do the final decision for us, but perhaps just seeing what others think on the manner might help us with what is our decision in the end. There is also the general problem that, at least for me, interests tend to stick around for a few months or years and then be replaced by others, which has lead to quite a few different plans for jobs or subjects to study by me in the past, and we somewhat hope to break that cycle at some point, but we don't know whether that will ever happen, though of course we can hope that now we are older than back then, change decreases. Note that this is the last year of school for us, so we should ideally choose what we want to do in a few months.

## Computer science
Computer science is what ultimately replaced electrical engineering as my favoured subject to study a few years ago. The exact direction I wanted to go in as a Master's degree changed over time, from cybersecurity over operating system design to cognitive sciences or computational neurosciences (interestingly enough, two of these can be shortened with `CS`, just like computer science itself), but the general preference stayed quite consistent over the years. We seem to be somewhat good at it, as well. Good marks in school, above than average programming skills, at least when compared to our offline social surroundings, relatively good results at contests and the like… but are we? I often feel a sort of envy when reading blogs or just at Git accounts of more experienced programmers than ourselves, which can, due to a perceived need to have those skills, even lead to depression… I currently can't imagine contributing to something with thousands of lines of code, writing a programming language parser, maintaining dozens of medium-sized projects or [writing something with practical use <q>in one afternoon</q>](https://cadence.moe/blog/2020-11-10-gemini) given [this one](https://gitlab.com/artemislena/debian-mergechangelogs-chversion-date) took me about 5 days. It also seems hard to get myself to do anything at all in this direction; I often find myself just hanging around in Matrix chats rather than trying to write that bot, script, game or app I have been wanting to do for weeks ‒ in other words, I tend to procrastinate when confronted with programming tasks. And, while anecdotal evidence

T.: Then why do ya bring that up at all?

DeM: Personal accounts may still be useful to show the possible, if not the probable. Hans Jonas' heuristics of fear come to mind.

L: Well, I guess we shouldn't overestimate it. These sorts of things can happen with any job. But if she wants to bring it up, well, I won't stop her.

T.: ¯\\\_(ツ)\_/¯

… we have seen two people expressing what could be seen as regret, mentioning either complete lack of interest or even hate for the subject (no blog post available for this one), or [mental health issues](https://cadence.moe/blog/2020-08-29-untitled-i).

So far, the plan was to try to work in academia and become a professor of computer science.

T.: And if that fails, work at a worker coop, if we can find one. We *don't* like traditional companies. Especially when they don't have unions.

We should also look for a place of work that emphasizes presence over home office, and freelancing wouldn't be good for us either, most likely, as our experiences with homeschooling showed; they lead to a lot of procrastination.

## Psychology
The want to get an MSc in cognitive sciences already brought us closer into this direction… And we recently found ourselves enjoying *Thinking, fast and slow* and *Algorithms to live by*, both of which written by psychologists (though, to be fair, the latter had two authors, one of them a computer scientist), as well as, it seems, reading more articles related to psychology than to computer science on Wikipedia.

T.: Ur not making a convincing case there.

Well, fair. But… we undeniably have an interest in this. Anyway.

<ins>Clarification: "Academia" in <q>Instead of going into academia</q> in the following paragraph means professional academics, as in post-MSc research, not studying at an university in general.</ins>

Now, an option post-studying would again be to go into academia. Like with any subject, it's up or out there, so if we don't reach the rank of a professor, we would probably have to work for a company. We don't think there are many coops looking for psychologists (but feel free to let us know if that's not the case), in addition, there are a lot of applications of psychology that we might view as unethical in certain contexts, such as marketing or election campaigning. There is another alternative, however. Probably instead of going to academia in the first place, we could do a training as psychotherapist. This would mean basically no home office at all and limited contact with distracting computers during the main work. We'd also directly work with humans, meaning something like the mental health issues mentioned in one of the blog posts by Cadence linked in the computer science sections of this post might be less of an issue. On the other hand, we're not sure how much bureaucracy we would have to deal with regarding health insurances and such, the fact we're autistic could limit our abilities when working with allistic clients (or increase them as we'll more actively and algorithmically interpret facial and gestural cues rather than relying on intuition), and if we don't get my depressions managed, that could be an issue as well ‒ having suicidal thoughts while dealing with a potentially suicidal client might be catastrophic.

<ins>Update: Some universities are starting to offer studying psychotherapy directly rather than psychology followed by a psychotherapy training. However, that likely means less flexibility, so studying plain psychology could be preferable.</ins>

T.: Tho I or so could front at that point, maybe, if that should ever happen, cause the rest of us don't have those.

Which brings us to another thing to consider ‒ being out as plural at work would be harder than in other jobs, most likely. It doesn't seem really recommendable to talk about one's psychological divergences with a client when *they* are the one supposed to talk about *theirs*.

<ins>Update: Another thing worth noting here is that we wouldn't completely miss out on "advanced computer usage" as we'd still administrate our servers and maintain smaller projects, whereas practicing psychology in one's free time might be harder.</ins>


Anyway, let us know what you think. We might publish a follow-up if we're any closer to making our final decision.

T.: <q>Final</q>? I've said it once, and I'll say it again: Don't forget the sunk cost fallacy. We could switch the subject we're studying in an emergency, even if that postpones stuff by some years.
