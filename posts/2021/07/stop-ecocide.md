---
title: "Stop Ecocide"
published_date: 2021-07-21 14:41:10 +0200
data: {author: "Dea ex Machina", cw: "Politics, environmentalism"}
---

Normally, neither I nor Laura deal with political issues, given Tanith and FantasyCookie17 are occupied with these enough for one body. However, this time, the latter was, in her own words, "too lazy to write this", and the former felt like I would deliver a better performance at explaining the content of this. This, along with the fact that I have not written much on this blog at all, lead to me writing this post today.

T.: Doesn't mean I won't chime in with a comment or two, ofc ^^

The search engine we most commonly use at the moment is [Ecosia](https://ecosia.org). Yes, we are aware some people might have a few minor or even major issues with its privacy policy, however, this shall not be the topic of this post. Recently, after having submitted a search query, a banner popped up, leading to [this post on their blog](https://blog.ecosia.org/ecocide-legislation/). Aside from the fact that their blog in general seems interesting and we might look into it more, that particular post was indeed of relevance.

It deals with the [Stop Ecocide](https://stopecocide.earth) organization, which is running a campaign to make ecocide, that is, the destruction of ecosystems, illegal. Such a thing might be associated with more or less esoteric such as the stereotype of people who hug trees…

T.: …but humans are more cuddly, right? Now's the part where xe explains why we're not cranks after all :)

Indeed. Consider the following: The earth is a large collection of various ecosystems, often interconnected in various ways. A lot of these are important for humans to survive. For example, ecosystems involving lots of plants, especially mosses, are crucial in slowing down climate change. Others may be important for preventing the bee population to diminish further ‒ and bees are necessary for most flowering plants to reproduce, which includes quite a few that are important as food sources for animals such as humans. 

T.: So no, we don't wanna hug trees, just make sure we can also hug humans tomorrow.

As previously implied, the ultimate goal of the campaign is to incentivize the creation of laws that are supposed to prevent ecocide. Of course, a law is not a panacea, but

T.: [Perfect solution fallacy](https://en.wikipedia.org/wiki/Nirvana_fallacy#Perfect_solution_fallacy). Laws act as a deterrent and can change societal views to some degree, that's better than nothing, right?

A first and rather easy step to help is [signing the petition](https://www.stopecocide.earth/become). [We have already advertised a petition in the past](https://artemislena.eu/posts/2021/05/trans-rights-petition.html), and, considering it all, it might become a habit that we keep on doing so where we think it would be sensible to advertise these.
