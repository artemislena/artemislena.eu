---
title: Systemic gatekeeping
published_date: 2021-06-29 02:49:00 +0200
data: {author: Tanith, cw: "Transmisic gatekeeping mentions, anger"}
---

So, this's basically a continuation of [my last post](https://artemislena.eu/posts/2021/04/gatekeeping-kills.html), so maybe read that one first ‒ or don't, this one's less sensitive or whatever.

Basically, we got the feeling that it's less the individual people and more the psychiatric system that leads to this gatekeeping, and false or misled beliefs in the heads of the individuals, rather than some actual vileness (which's a poor explanation anyway, see Hanlon's Razor). Hence the title. So, that's why I'm still not going to mention names like Mr. Haase, Ms. Rothe, or, for good measure, Ms. Wagner ;)

## "But that would mean saying 'I'm trans' would suffice"

Thanks, Captain Obvious. That's where the truly vile plan of ***THE TRANSGENDERISTS*** will lead us.

![A meme showing a futuristic eutopia, captioned 'Society where that plan worked'](society-where.webp)

## "It's so you don't sue us in 10 years"
Ah. Because experts, especially in psychology and such are so excellingly great at doing long-term prognoses and *at least* as good as the [statistics](https://rationalwiki.org/wiki/Gender_transition#The_Myth_of_High_Detransitin_Rates) (hint: they're not, see chapters 20 and 21 of *Thinking ‒ Fast and Slow* by Daniel Kahneman, I could list all the sources he used in there, but he did that already for me (thanks Daniel <3) so why). And because informed consent isn't a thing.

## "I'd like to understand you better"
Literally, keep out of our private stuff if it has nothing to do with gender and transitioning. Not joking. GTFO. And you know how friggin exhausting that sentence completing game is? Have you ever done it yourself or do you just test your torture methods on your subjects? Literally, this is small-scale psychological warfare.

![A trade offer meme, captioned "I stop: my blogging warfare You stop: your psychological warfare"](trade-offer.webp)
