---
title: "Client defined condition naming"
published_date: 2021-12-25 00:46:48 +0100
data: {author: "FantasyCookie17", cw: "Medical system, mental health"}
---
So… first of all, despite the possibly technical sounding title, this isn't a post on programming, networking, or the like… If you came here expecting that… sorry to disappoint you… This is on my (and actually our) personal takes on nosology, clinical psychology, and psychiatry… It's yet another post I've been wanting to write for a long time… and it might require some further research… Anyway, let's get started.

## Definitions
- A *condition* is a diagnosable phenomenon or set of phenomenons in an individual that diverges from what is common in the average population.
- A *client* is an individual, usually with a condition, seeking help by a therapist, who may for example be a psychiatrist or psychologist.
- A *name* is a label applied to a condition. For example, this might simply be "condition", but could also be "disorder", "illness", "syndrome", or "disability".

## The debate
There's a variety of views on how to handle psychological conditions, including how to label them…

For example, I mostly align with the ["neurodiversity movement"](https://en.wikipedia.org/wiki/Neurodiversity), ND for short, seeking depathogenization of some of these, where it seems to make sense, and generally emphasizing that divergence is a natural and not necessarily negative part of human diversity (though I should note it's quite heterogenous)… Sometimes, more radical elements who may or may not identify with this movement might call for complete removal of certain conditions from diagnostic manuals, however, given how many neurodivergent individuals tend to benefit from, for example, how a diagnosis might explain their "otherness", I don't think that's sensible in our current society… Yes, it worked with homosexuality… but that's a thing that never requires treatment of any kind, by itself (of course, discrimination and social stigma may cause conditions that do, but that's a different thing)… And it's also something that's fairly easily recognizable by laypeople, so it doesn't usually cause these undefined feelings of otherness and alienation without explanation, as undiagnosed neurodivergencies may… I think a favoured condition name here is "condition", at least it's what I normally use, and what I've seen people using…

There's the [r/ADHD view](https://reddit.artemislena.eu/r/ADHD/comments/ms95dl/radhds_position_on_neurodiversity/) ‒ I feel this is based on an unfortunate misunderstanding… basically, they more or less banned words like "neurodiverse" from being used on the subreddit, as they feel… if I got it correctly… that depathogenization and the social model of disability will remove their ability to access pharmacotherapy and the like, and that the ND movement too strongly emphasizes these? Also, they feel points mainly made by the group in the next paragraph are too much accepted by the general ND community… which I also don't agree with, at least for the spaces I've been in, *to some degree*… You might usually see terms like "disorder" being used here, at least I'd assume that from using "ADHD" (whereas the second "D" stands for "disorder") for their subreddit name, and also from what I can tell from the linked post…

… and that brings us to the third group, colloquially known as "aspie supremacists" ("aspie" is an abbreviation for an individual with so-called "Asperger's syndrome", one of the old subtypes for conditions on the autism spectrum, which I happen to be diagnosed with, by the way). They… basically feel that certain (though usually not all) conditions may put them in a position of (evolutional) "superiority", that they are somehow cognitively more advanced than a neurotypical person… I think this is a rather dangerous view, and you may often see it being frowned upon by other people in the ND community as well… The reason for that is mainly that it leads to othering, and viewing yourself as "superior" to others… well, I hope I don't have to explain why that's a bad idea… Something that might be used as a condition name here is "gift", for example… Something I wouldn't generally encourage, for the record; something more neutral like "condition" should be preferred, in my opinion, in the majority of cases…

Last but not least, there's what I'd call the "clinical perspective", which, however, does not apply to all clinics and medical professionals, as some might have views closer to the ND movement, for example…

T.: Then why call't "clinical"?  
fc17: In lack of a better word… Do you have one?  
T.: Hm. "Nosological" maybe? Dunno. "Clinical"'s fine enough for the moment, Ig.

This mostly emphasizes negative aspects of a condition, and you would probably see the condition names found in diagnostic manuals here… "Disorders", "syndrome", and the like…

## The problem
T.: Aka why even care?  
DeM: The question of why things matter is indeed one that often is asked to be solved.

So… I see three reasons here… One is the issue of framing, that is, framing something as a "disability" might negatively impact how a client experiences their symptoms, and lead to social stigmatization, for example… Secondly, there's the general matter of well-being; a client may feel offended or discriminated by someone using a negatively connoted term for something they might view as more or less a harmless set of personally traits… Thirdly, a label may impact how a therapist wants to treat a condition, if at all (and whether the client's health insurance wants to pay for this treatment), whereas an "illness" is more likely to be treated with medication, than, say, a "gift" (again, not a term I'd use)…

## My proposal
The title already says it, I think… Let clients decide whether their "ADHD" is actually a "disorder", or, I don't know, a "quirk" (just a random example)… Also, for similar reasons as above, "clients" may voluntarily label themselves as "patients", however, this shouldn't be imposed by their therapist. Just like the "disorder" label shouldn't be. "Condition" and "client" are relatively neutral terms without specific positive or negative connotations, so they're fine as a default, I'd say… And just like therapists shouldn't, online community moderation, family, or anyone else, shouldn't either… This way, we might in fact dissolve this entire debate explained above…

Of course, there's limits to this… A client with a certain condition may decide how their own condition is named, but not how those of others are, even if they share a diagnostic label, such as… "Tourette's", or "autism", or "ADHD", as that would kind of defeat the entire point of this… Also, while this may be controversial, I don't think it generally works too well for… "heavier" conditions, such as major depression, or psychosis, where a client's capability to reason and perceive their social environment's perceptions of "reality" correctly… So the "clinical view" may be preferred here, in fact… "Cognitive impairment" is a interesting one as for this… it certainly interferes with some of these abilities… but with the right environment, the… negative impact on the wellbeing (harm) of a client or their social environment tends to be rather low… So is it really a "disorder" then? I'll refrain from answering, currently…

So… this post should be made somewhat useful, right?

T.: DUH.

That, in this case, should be concrete advice for therapists and clients, I guess… Thing is… I'm not a therapist myself, and only have a vague idea of how therapy and the like works from a therapist's perspective… (after elaborating on this in my [Career choices post](/posts/2021/08/career-choices.html), we basically discarded computer science, now being more firm towards our choice of wanting to study psychology, which means that our knowledge on this should increase over the years) However, some general things I should be able to talk about… As for therapists, don't try to diagnose anything if you don't assume the diagnosis of a condition is going to help your client (you may assume this if you might offer treatment specific to the condition, or if you notice they feel alienated due to their symptoms, for example), and if you diagnose a condition, you should explain that, even if the official label is something like "disorder" or "syndrome", the client is free to use a more neutral or otherwise different name for their condition, and respect their decision when you talk of their condition. As for clients, if you want to, and feel you're being subject to misconduct, simply insist on these being your rights, and explain why you feel it is important to you to have your condition labeled correctly, possibly using information you gathered from this article. Feel free to link this post where you feel it's appropriate…

<ins>Update: Wanted to mention this originally… The advice to refrain from diagnosing conditions is also rooted in the fact that psychological diagnoses are notoriously unreliable, as shown by for example [how easy it is to fake psychosis](https://www.semanticscholar.org/paper/THE-SUSCEPTIBILITY-OF-THE-RORSCHACH-TO-FAKING-OF-BY-Albert/ac6f6c5d79f9860a483272458b74e90a063d7aaa)</ins>

Opinions stated in this post are, as always, subject to change. There may be follow-up posts as well…

L: Hm. Didn't say anything in here. Don't see where I would, though. Oh, wait. Merry Christmas and that sort of stuff to you all, I guess? We all know 2021 was a bit, uh, not so great in some regards. Hope 2022 will be better. Who knows.
