---
title: "Gatekeeping kills"
published_date: 2021-04-20 16:15:21 +0200
data: {author: "Tanith", cw: "Mental health, suicidality, anger directed at headmates, transmisic gatekeeping"}
---

You may have already seen me on the last post. I'm Tanith, and if you're not up to speed on the entire thematic of F. being plural, check [this](/plurality.html) out. Now, for the part of this post after this paragraph, I'll have to issue a big content warning. I am absolutely pissed (American English, mind you), and this post will reflect both that as well as mention serious mental health stuff. I'm also known for putting humour into all sorts of stuff, which may be considered inappropriate in this context.

## What happened
We visited a psychiatrist (not going to name anyone, Dr. Robert Haase, am I? Stuff like that could throw a bad light on your reputation ;) ), hoping to finally receive that cease-and-de-cis order F. had wanted for so long (from her perspective). However, the guy basically told us we'd have to wait at least half a year or so, *if we were lucky*, and then we'd still need a second one. Upon that, F. had the *glorious* idea to announce she'd not wait that long, and would instead kill herself today, in order to "make a change" (as in, martyr bullshit or something). The situation then only deescalated by me and the others sending intentions to F. to change her plans, and Mr. Haase suggesting she could instead try to get politically active.

## To F.
Absolutely shitty behaviour. You would've killed us all. I mean, I get why you wanted this, but you need to keep in mind you're not alone in here. You've not even changed your mind really, have you? Still not feeling guilty for *wanting to murder three other personas as "collateral damage"*? Me taking over control of the body and most of the consciousness is what you get from that. No, I'm not letting you think right now. I don't want to deal with that suicide garbage again.

## To the gatekeepers
You're not an inch better. May you dissolve in acid. bUT ThiNK oF THe ChILdrEn, sure. I'm legally a child, and I would've almost gotten murdered in the process. Also, show me the statistic backing you up. I've [got these listed here](https://rationalwiki.org/wiki/Gender_transition#The_Myth_of_High_Detransition_Rates). Shifting blame is a thing you really like, don't you?

> threatening with suicide

Yeah, no. She was serious, and she wasn't threatening you ‒ she very much knew that you wouldn't change based on that. Furthermore, she had already planned that sort of reaction way ahead (at which point we also didn't appreciate it, to say the least). She was merely hoping for a change in society as a reaction to the incident that would've happened. As much as I hate her right now, I still think she deserves fair treatment.

> inexperienced psychologist who made you false hopes by giving you an indication way too early

F. was disappointed she didn't get it earlier. If at all, that one destroyed hopes (at first). Don't think she is the right one to blame here whatsoever. But sure, make her feel guilty if she ever hears of this.

> bUt THe GUIdElInes

Lmao. You literally admitted yourself there were no official guidelines for minors, just some <q>a few experts</q> made and don't seem to show the public (of course, such a thing would *never* allow *anyone* to abuse their authority). There is a far less restrictive one for adults, but, you know, your brain suddenly becomes mature the day you get 18, [so we need to gatekeep anyone below that](https://en.wikipedia.org/wiki/Ageism). Amazing.

Oh, and bringing up non-issues is also a *great strategy*. Because, you know, we might try to hide that we're trans once transitioned to a certain point (not like F. literally has a pride flag in her display name and the fact she's trans on this frigging website), or, even worse, ***we might want surgery once into hormone therapy***! Clearly, a crime. And, of course, nobody of us would *ever dare to think of SRS* now. And, uh, what was your goal again? Accompanying the transition? Might have better success with that if you don't make everyone hate you for being a gatekeeping transmedicalist so that they leave once they got the indications and stuff they need. And, uh, giving England as an example… Not great. The NHS is infamous among trans people for committing transphobic gatekeeping (source: ask any trans individual in the UK currently trying to transition).

## To all others
Is this a call for help? Probably not. If it was, me might get forced into a psychiatric institution, and no thanks, we don't need that trauma in addition to probably having to repeat this year in school. (Also, probably the least of our issues, but knowing what a picky eater F. is, we might starve.) No, we're not insane or anything. Plurality is perfectly normal, at least it wouldn't quite fit with the narrative of the "insane plural system" given F. is the one who has mental health issues, and the others the one who tried to stay somewhat sane in spite of those shitty gatekeepers and stuff. Not sure whether reading this makes you feel any better. Probably not. At least you now know how awful gatekeeping actually can be, if you didn't so far. ¯\\\_(ツ)\_/¯ Guess I just needed to rant for a moment.

Thanks for attending my TED talk. See ya next time.
