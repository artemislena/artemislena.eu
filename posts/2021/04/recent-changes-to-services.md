---
title: "Recent changes to services"
published_date: 2021-04-10 18:46:13 +0200
data: {author: "FantasyCookie17"}
---
## Domain
I bought the domain `artemislena.eu` in a sale at netcup. My old domains `fantasycookie17.cf` and `fantasycookie17.onederfultech.com` still redirect here. My Matrix server will move to the new domain, more on that below.

## BorgBackup
I ditched all my plans with NextCloud and set up a BorgBackup server, loosely following [the instructions by Sun Knudsen](https://sunknudsen.com/privacy-guides/how-to-self-host-hardened-borg-server). The service, like my Matrix server, my Mumble server and the offer to host files for download on my website, is available as a "friendcloud", that is, public registration is not possible (thus preventing bulk registration and such), however, if you ask me, I know you somewhat, and have no specific reason to dislike you, you should be able to get an account.

## Verifying
There have been some additions to [my Verify page](/services/verify.html), most notably my own Minisign key, which is now used to sign both said Verify page, and the newly added [canary](/services/canary.html), which, while not really needed for any reasonable threat model on a small page like this one, is still a nice project at least for me personally. The favicon of this website was changed a while ago as well, and now contains a QR code which contains my public Minisign key.

## Infrastructure changes
As you may or may not know, this website and my Matrix server currently run in VMs on an AMD64 machine with a Ryzen 3 2200G and 16GB of RAM, using Fedora as VM host and Void Linux as guests, BorgBackup runs on an Odroid HC4 with Armbian Unstable, and my Mumble server runs on a Rock64 4GB with OpenBSD. Now, to both get away from the AMD64 architecture, and to not have to rely on KVM for isolation, I ordered a RockPro64 4GB along with an M.2 SSD and a Rock64 2GB, as well as some eMMC for both. My Mumble server will move to the Rock64 2GB, my website will be served from the Rock64 4GB, and I will set up the Matrix server for my new domain on the RockPro64, with the DB and media store stored to the SSD. I will likely use Dendrite for that, unless Conduit becomes a viable option before the machine arrives and I get to set it up. Both of the Rock64s and the RockPro64 will be running NixOS.

## Announcement for users of my Matrix server
My old Matrix server will be shut down. You will of course have the option to use an account on my new Matrix server, however, it is important that you properly reset your power levels in all rooms where yours is raised (i.e. where you are an admin or moderator) and have a backup account for these rooms on another server with the same power level so you can promote yourself again once the new server goes up. Please DM me so we can discuss further details.

## GTRR
The GTRR has moved to [its own subdomain](https://gtrr.artemislena.eu), though you can also still reach it under the old path. Its source code is now also mirrored to GitHub and GitLab, so contributions can be done there as well.
