---
title: "Debunking transphobia for fun (and no profit, sadly)"
published_date: 2021-02-06 02:36:40 +0100
data: {author: "FantasyCookie17", cw: "Transmisia mentions"}
---
Today, someone in one of the rooms I moderate had to get banned for spreading homophobia and transphobia. In the argument preceding the ban, he shared a video with the lovely title ["Doctor Destroys Transgenderism with Simple Science"](https://invidio.us/watch?v=uFtK1qsh1nc). I did not actually bother to watch it back then, mainly due to the fact it has a length of 27 minutes and 17 seconds, however, as if there was nothing else to do, I decided to watch it now anyway just to see how fallacious it is. Strange hobby to do in the middle of the night, I know, and probably not very useful, but whatever. I felt like it. I will just go through the main arguments given, and show how should be considered inaccurate, wrong or misleading. My vocabulary on logical fallacies and such mainly comes from [RationalWiki](https://rationalwiki.org); if you do not know a term, look it up there.

## Initial red flags
Not that I would be *prejudiced*, but the video was made by a channel that presents itself as some ultraconservative trash, with the person interviewed being a pediatrician ‒ not someone who could actually be called an expert on the field of transgender topics. It claims <q>simple science</q> ‒ science is never simple; if you try to oversimplify, you just end up with tons of cognitive biases. Humans are *not* good at intuitively interpreting statistics, since intuition tends to be lead by heuristics rather than the actual statistic data at hand. Oh, and the comment section is an atrocious example of almost pure transphobia, with few people displaying any criticism towards the content. The top comment labels transgenderism as <q>orwellian</q>, regurgitating the party slogans from *1984*, with no connection shown to how any of this is related to being transgender. Ironically enough, George Orwell was a democratic socialist, which is quite on the opposite end of certain political spectrums when compared to conservatism. Generally speaking, this shows you the video is probably a waste of time, but again, no prejudice, so I decided to watch it anyway.

## <q>Transgender ideology</q>
She starts with wrong assumptions right away. <q>Transgenderism</q> is not an ideology; there is no hidden agenda that transgender people follow. All what being transgender means is to not feel like the gender one was assigned at birth matches one's actual gender identity.

## <q>Wrong bodies</q>
This is a straw man. Transgender people often feel like they may have been born with the wrong sex characteristics to reflect their gender identity. This does not mean their body is overall wrong, similarly to how being short-sighted and thus needing glasses does not create the feeling of a <q>wrong body</q>. And, sorry, but what do twin studies have to do with this? Twin studies can help showing whether the causes of some phenomenon are rather genetic or sociologic, but that is not even the question here, now is it? Also, none of said <q>twin studies</q> is mentioned.

## <q>Indoctrination</q>
I have yet to see this happening. Are you nutpicking or something? Nobody is <q>indoctrinated</q> to the idea of being transgender. Sure, there have been some cases where people have been talked into it by cranks who frankly are no better than transphobics, but again, that is nutpicking. Not something that I have seen generally being supported in the transgender community. Cases of detransition are rare, anyway, and most of them are due to lack of social acceptance (a.k.a. transphobia, which is perpetuated by videos like this one, ironically). In either case, neutral education on the topic might be helpful ‒ any sense of gender identity should emerge endogenically though, without pushing anybody into being either trans or cis.

## <q>Destroys ability for reality testing</q>
Sorry, what? The example she used here might be one where, again, better education might help, as there is a difference between crossdressing and being transgender, but, apart from that, this seems like a non sequitur, essentially. What follows is some anecdotal evidence where, yet again, undereducation seemed to the problem.

## Health risks
Yes, there are certain health risks associated with hormone therapy and such, however, these seem severely exaggerated by the speaker. I have not found any sources for it causing diabetes, and neither did she provide any. And nobody is being forced to anything; in fact, hurdles for getting any sort of medical transition are often too high for those who legitimately need it, and, in any case, this is based on informed consent.

## <q>Watchful waiting</q>
This is why you start with puberty blockers rather than hormone therapy right away, duh…

## <q>We must treat our patients according to their biology, not according to their perceptions</q>
Biology is a wide field… Certain factors are genetic, others are hormonal, and others are psychological. Treatments are adjusted depending on which factors influence a disease or disorder. What is she even arguing there? 

## <q>Enforcement of Newspeak</q>
George Orwell's concept of <q>Newspeak</q>, as described in *1984*, focused on the removal of words. Using more inclusive language is quite the opposite, however, as in total, more words will be used, and they will have more accurate definitions (as opposed to conflating terms like "sex" and "gender").

## <q>Trans people are delusional</q>
Well, no. Most of them are well aware that their pre-transition body does not match their gender identity in its current state. Delusions can be corrected, however, so-called "reparative therapy"/"conversion therapyu" never works.

## <q>fMRIs prove nothing</q>
So, the reason stated here is that behaviour changes the brain structure ‒ this may very well be, nevertheless, there are documented differences pre-hormone therapy, and given that hormone therapy is (ideally) given shortly after the realization one is trans, if so desired, it means there cannot typically be any behavioural changes that have been persistent for long enough to seriously change larger things about brain structure. Furthermore, brain structure is not everything, any diverging claims are a severe underestimation of the complexity of neurology and cognition. And there very definitely are statistical tendencies, however, the idea of a "male brain" and "female brain" is somewhat flawed.

## <q>Twin studies prove gender identity evolves postnatally</q>
There can be changes happening during pregnancy… And no, even identical twins do not have 100% identical DNA.

## <q>Sex is in the DNA</q>
Intersex people: *exist*

To be fair, intersex conditions have mostly genetic reasons, but nevertheless, sex is *supposedly* caused by the karyotype, yet, intersex people often have the karyotype of one sex (unless we are talking about Klinefelter's syndrome), and only their phenotype does not quite fit that. With neurology being so much more complex than simple physiology, why would transgender people not be a thing?

<ins>Update: Actually, only a single gene, the SRY gene, which encodes for the [testis-determining factor](https://en.wikipedia.org/wiki/Testis-determining_factor), is responsible for the development of the male phenotype. So much as for <q>several thousands of genes</q>.</ins>

## <q>Gender refers to a concept of grammar</q>
Well, it used to, but languages are not static. So what? And no, they did not "pull it out of the air", they used an already existing word to help describing a condition that likely has existed for a very long time, but simply was not recognized before.

## <q>Intersex is the exception, not the rule</q>
True, so is being transgender.

## The genetics argument
No human is actually completely binary, genetically seen. For example, during pregnancy, cells may be exchanged, and live on in the body of the mother or child for decades by continuing to reproduce there, leading to men having some XX cells, and women having some XY cells, especially after being pregnant with a male child. Source? I read an article about it a while ago… Given that said physician does not state any sources, I do not see why I would need to either, even if that makes my argument somewhat weaker. This article was written for my personal entertainment. I do not intend to scrape the internet for that kind of stuff right now. If you find something for this, let me know, I will add it.

## <q>Violation of human nature</q>
Any sort of medical treatment is, in a way. So what? We are using tools to, in a sense, augment ourselves. What is wrong with that? This is a very human behaviour, after all, and thus natural. Appeals to nature are a logical fallacy, anyway.

## <q>Doping</q>
Well, perhaps there should be proper regulations in sports for transgender people and for hormone levels in general, but this seems to be simply a lack of recognition, a loophole in the rules, possibly.

## <q>Now the drag is hormones and surgery</q>
Drag, crossdressing, and being trans are separate things. Conflating them does not make your position more credible.

## <q>It's written in our DNA and every cell of our body</q>
Oh, so you fully understood every biochemical interaction happening there? Well, please be so kind and share, you would be advancing science by centuries, possibly millenia.

## <q>Mistake of equality meaning same</q>
This is an argument on semantics that does not even have a place in this debate. Trans people are embracing gender diversity and identity, what the speaker in the video is doing is rejecting it by treating it as a fully binary trait only determined by physiology, which essentially reminds a bit of sexism? Amazing.

## Pronatalist ramblings
Whatever. I do not care, really. Having many children is effectively decreasing quality of life for other children, as it leads to overpopulation, so… Also completely missing the original point, just like the previous "argument".
