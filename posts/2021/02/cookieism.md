---
title: "Introducing: The Church Of Cookieism!"
published_date: 2021-02-12 00:42:08 +0100
data: {author: "FantasyCookie17", cw: "Religion? (mainly a joke)"}
---

<ins>Update: The joke has worn off, there are more interesting and elaborate parody religions, such as Pastafarianism, and so on… The link mentioned is dead and the menu entry has been removed.</ins>

~~Recently, in the main Techlore room, mazerfaker, DriftNotSkid and I were having some fun and joking around a bit after I said "Tell me something new" in reaction to a screenshot of one of those fingerprint testing sites stating "Your browser has a unique fingerprint", which lead to the creation of one of those parody religions ‒ The Church of Cookieism. It is loosely based on Christianity and other Abrahamic religions… [But see for yourself](/cookieism.html). I also added that link as a menu entry. I'm happy about suggestions about it ‒ certainly, more characters and such should be added, and I would not even be opposed in referencing other religions (note: all of this is a joke, and we are not making fun of you for believing or not believing in anything).~~
