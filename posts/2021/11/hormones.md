---
title: "Hormones! :D"
data: {author: "Tanith", cw: "Hormones/medication"}
published_date: 2021-11-07 00:51:40 +0100
---

So, as ya might know, we're trans, and we've been on E2 (estradiol), since the 30tha August now, and we started on cypro (CPA, an antiandrogen) on Thursday this week. Which's pretty exciting, so I wanteda make a post on it ^^

Well. Not much morea say on this. We waited for months, basically (see my "gatekeeping" posts), which's too long, but still shorter than what a lotta others needa wait (my condolences if ya'vea go thru, whatever, the UK system, or worse), we ended up havinga get a prescription from a gynecologist in Berlin, which's somewhat far away from where we live, but apparently the endocrinologists here didn't wanna accept the diagnosis our psychologist did for us, demanding that psychiatrist instead -_- Anyway, he gave us a prescription for transdermal E2 gel back then, two dosages Gynokadin a day (which contain 1.55mg E2 in total), which was apparently enough for normal E2 levels (our first blood test after starting was messed up, gave us *way* too high levels, second one seemed fine tho), but T suppression wasn't enough. However, he was hardly available (he's technically retired already, only appears in his office like, whatever, every few weeks or so), so t'took a bit longer than originally thought, but whatever, we're where we want now :) T levels weren't too high, so we got a rather low dosage, 5mg cypro (half an Androcur 10 pill/day), prescribed. The gynecologist who took over his clinic sent us the wrong prescription (another packa E2 gel, even tho we've enougha that atm ;D), but since F.'s parents're physicians, they could get the meds from their apothecary anyway, promising they'd bring the right prescription as soona we've got it. So, yeah, E2 levels up, T levels going down in the moment I'm typing this maybe, pretty nice, ain't it? ^^

Some pics ("proof" ;) ):
![An Androcur 10 package](./androcur.webp)
![Three times Gynokadin gel, in the middle an unpacked bottlea it, on the left and right bottles that're still in their cardboard box](./gynokadin.webp)
