---
data: {author: "All of us", cw: "Weird thoughts? Mental health stuff, some general negativity"}
title: "Fragments 2"
published_date: 2021-11-07 00:51:15 +0100
---

> [Context: Wikipedia says schizophrenia lowers life expectancy by about 20 years]  
> fc17: So schizophrenics die more often…  
> T.: What? There's far less schizophrenics than other people.  
> fc17: But if we had the same amount of schizophrenics and others in the population…  
> T.: If half the population were schizophrenic, we'd've a problem.

fc17: Note: This isn't quite politically correct (I mean, these are literally thoughts written down, not public speeches), I guess, so let's clarify some things… Yes, you might find slightly different statistics on life expectancy, though a reduction by 20 years seems like a generally accepted average estimate, and there certainly isn't a monocausal explanation for it… And no, we don't want to increase stigma against people with schizophrenia, "problem" here just refers that *most* (not all, I guess) of people diagnosed with schizophrenia suffer from rather problematic symptoms a lot of the time, as, as you might know, while "positive symptoms" (i.e. psychosis) are relatively easily to treat, usually, "negative symptoms" (affective and cognitive) currently aren't (though newer medication like [Lumateperone/Caplyta](https://en.wikipedia.org/wiki/Lumateperone) might help here, as it is also known to decrease negative symptoms, anyway, I digress)… The joke here doesn't derive from schizophrenia, but from me not thinking through the implications of a statistic thoroughly, and then my attempt to "fix" my mistake through a hypothetical situation basically worsening things, as Tanith pointed out…

> L: If I speak, is that because *you* want me to speak, or because *I* want to speak?

> L: We're just flowing along the road.

> T.: What?  
> fc17: Sorry… tired… maybe tiredness makes certain neurons less activated then usually, so other neurons become more activated relatively to those, leading to those nonsense thoughts?  
> T.: Hm. Maybe.  
> DeM: Of course we should not be too quick to assess such a hypothesis as "true" before we have tested it.

fc17: Keep in mind I literally thought this while tired. Am I at least somewhat close to the truth? Is there even any research on why tiredness may lead to nonsense thoughts rather than just less thoughts?

> [Sees a lot of lanterns in one area]  
> T.: Wow look! Pretty badass, ain't it? "Look at how much power we can burn", lmao.

> T.: Trams exist. We live in the best of all possible worlds.

T.: Yeah, not a typo. I said "trams" there, not "trains" or "trans".

> fc17: I think we basically make up our persistent selves by using memories…
