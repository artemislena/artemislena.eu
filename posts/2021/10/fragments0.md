---
title: "Fragments 0"
published_date: 2021-10-06 18:15:55 +0200
data: {author: "Tanith", cw: "Weird thoughts?"}
---
Ah, [bathroom thoughts](https://existentialcomics.com/comic/14) ^^ So, we came up with this: Basically, others like putting, yk, comics, or short stories or whatever on their sites, but those require like, writing some fiction usually, and that's not ideal for us cause imagining a character in our head… well, we're four of those basically ;D But we do produce a lotta thoughts over a day, usually (like most people I hope?) and we thought it'd be a nice idea'a write somea these down, cause, Idk, they're sometimes fun or some philosophical observations or both, or whatever. I was the firsta write some down (and, cause they're not always super clear, and we don't language everything we think all the time, inserting some stuffa make them more clear, not that they'll be super clear anyway), and we gave it the super creative title *Fragments* (cause, yk, stuff's sorta outta context mosta the time and what not). Might continue with further parts at some point, regardless, here're the first ones (or 0th ones ^^), chronologically ordered (as if that mattered):

> F.: So, we realized that what headspace looks like is actually what the world looks like to us, and… our brain just puzzles things together, but it can't do that as quickly in headspace [because there are no eyes to help], so [filling in details or the large picture is a bit delayed]… Do we agree on that?  
> DeM: Yes, indeed.  
> fc17: [Should we even have a consensus as a system?]  
> [References to some thought trains, prior thoughts n stuff, I starta think sth]  
> fc17: Wait Tanith, you're not writing that down!  
> T.: Yes I am! ^^ We agreed we'd write *something* down, and we needa start somewhere!

> L: Hm, I haven't said anything here yet.  
> T.: Feel freea speak, ur on microphone! ^^  
> L: I don't think I'm prepared for a speech just like that. Also there is no microphone. Oh, come on Tanith.  
> T.: I'm loving this.

> fc17: … or they could [instead of double blind trials] tell the subjects the physicians were psychotic so the subjects don't trust their hints telling them whether they're getting the placebo… Tanith, you can't write basically all of our thoughts down!  
> T.: Yes I can. Uh, could, at least. Anyway, doing it with this one rn.

> fc17: So *Rain* is going to be over soon…  
> T.: All girls are mortal, comrade.

> T.: So, when I say I "read this book", that's not technically true, I just remember the *experience* of'ving read it. Ya read it and I didn't exist, but I've accessa that memory.  

> [While playing Veloren]  
> T.: Oh, a Salamander. Sorry, ur boring, I'm not gonna kill you.
