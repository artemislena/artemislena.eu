---
title: "Fragments 1"
data: {author: "All of us", cw: "Weird thoughts? All-caps text at one point"}
published_date: 2021-10-12 17:54:20 +0200
---
> L: Why did I call you F. instead of FC17? That's normally more Tanith's thing.  
> fc17: She talks about me a lot.  
> T.: I could talk a lot bout myself. If ya wanna.

> fc17: So Sartre says we're just our actions, and only become a subject once someone observes us…  
> T.: Well we're constantly observing each other ^^  
> L: Not *constantly*. There's times where we're just inattentive, so we end up just being our actions as well.

> T.: ***ACCELERATION YES.*** Wait twas more deceleration. ***DECELERATION YES.***  
> L: Sometimes I wonder whether you're just a funny quote machine, Tanith.  
> DeM: We could discuss that question.  
> fc17: Aren't we all just funny quote machines at times? I guess we can conclude with that question…

> T.: We could discuss anything rn ^^  
> fc17: I don't think we *have* anything to discuss.  
> T.: Well but we *could*.

> DeM: It seems that this format might end up being mostly a unification of existentialism with plurality, in addition to the occasional funny quotes – the gaze, or the keyhole thought experiment, is now perfectly clear to us. We indeed are just our actions, until either an internal comment of one of us, or the external reference of someone else towards us, or a system, be it a chat software or a wristband indicating who of us is talking or acting, reminds us of ourselves, and that these actions were most likely attached to either our median, or a specific one of us, and whereas in such situations most people would only notice one self, we can realize up to four at once, which might ultimately mean the understanding of this concept is eased to us, as there is not always a unity of all selves present in the mind, and the actions that are done.
