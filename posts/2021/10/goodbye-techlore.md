---
title: "Goodbye Techlore"
data: {author: "FantasyCookie17", cw: "Community moderation"}
published_date: 2021-10-21 23:34:55 +0200
---
Yesterday, I stepped back from my role as a moderator in [Techlore](https://techlore.tech)'s Matrix rooms and left all of them. This, apparently…

T.: Aw c'mon. Ya shouldn't be that surprised bout it should ya?

…lead to several people being unhappy with that turn of events… Someone even made this meme:

![An image saying "Techlore when FC17 leaves", showing some flames in the background](./fc17_meme.webp)

So I thought I'd explain a bit… Basically, the only reason I was still around in the Techlore rooms was that I felt responsible as a moderator, as really, I had lost a bit of interest on the topics… Not that I think privacy isn't important at all anymore, it's just… not as interesting as it used to be, I guess… And as I realized I didn't really want to judge on what I could have moderated ‒ I felt there were a lot of possibly harmful actors I couldn't really do anything about as they weren't directly violating any rules ‒ and that I wasn't active enough to moderate when it would have been "easy" (as in easy to find rule violations), such as during spam waves, I didn't really see why I would want to be a moderator anymore… And these rooms were eating time nevertheless, time that could be spent for other things…

T.: FTR, me being the only other onea us who ever was in TL: I'm still in chat and memes for the moment, cause I don't wanna leave memes rooms usually (duh ;D), and chat's usually pretty chill. Left hot topics tho, for similar reasons (except I wasn't a mod).

A reminder: I'm still on Matrix in the other rooms I was in (you'll find me in Neurodiversity, the Tulpa Alcove, OS Security, or the room for this blog, for example), and I can still be contacted [in various ways](/contact.html).

T.: She's also in Anticapitalism Basedment but ya needa ask me for an invite for that cause it's only for based people ;)

Yes, I also felt sad yesterday, but I am pretty sure that's little more than loss aversion bias, and right now, I'm fine, feeling it probably was the right step… I guess that's it, then… I'm not mad at Techlore or anything else for anything, and I wish that community will continue to do the good work. If you're missing me, or want to talk, well, you know where to find me…
