---
title: "Petition for trans rights in Germany"
published_date: 2021-05-28 09:53:30 +0200
data: {author: "FantasyCookie17", cw: "German trans politics"}
---
Just wanted to let you all know there was [this petition here](https://www.change.org/p/selbstbestimmungjetzt-tsgabschaffen), basically aiming to abolish the outdated TSG (<q>Transsexuellengesetz</q>, <q>"transsexual law"</q>) in favour of a more modern self-determination law. As currently, legal name and gender changes are extremely expensive (1000€ and above due to the requirement of two evaluations by mental health professionals) and time-intensive (about a year), this would be a huge step forward. You can also sign if you are not a German citizen, in which case, a translator like [DeepL](https://deepl.com) might help you to determine *what exactly* you are signing.
