---
title: Major overhaul
published_date: 2021-05-29 22:24:20 +0200
data: {author: "FantasyCookie17"}
---
I've talked about wanting to write this article in Bloggers Gathering for quite a while now, so, here it is. While it was clear I wanted to make some changes, as also stated in a previous post on here, some of the changes I made went differently than planned, while others were abolished in favour of an alternative.

T.: A lot of them, actually. Or should I say all? ^^

Well, yes. Anyway, I will go into detail below.

## Website
This list is probably non-exhaustive.
* I stopped wanting to write my own SSG in favour of improving my Cobalt (Liquid) and Caddy templates.
* The [archives](/posts) now have icons to indicate whether it is a file or folder. Public domain, made by the Noun Project (or so) and published on Wikimedia.
* The menu button now uses `line-height` in addition to `padding` in order to improve the horizontal centering on all browsers. Still not perfect, but Grid seems too complex, and Flex does not work as expected.
* The [source code](/source.html) now has its own Git repo.
* The site has become more open about our plurality, favouring "we" and "Artemis" over "I" and "FantasyCookie17" where applicable.
* Thanks to running Caddy outside of a sandbox, MIME types should now be indicated correctly.

## Infrastructure
As announced in said previous post (T.: How about [just linking that one](/posts/2021/04/recent-changes-to-services.html)?), apart from the BorgBackup server, all servers now run on Pine64 hardware. We ended up not using the 4GB Rock64 (having a spare one is also nice, however, I guess), as some user(s) (T.: Should we say who? DeM: It might be wiser to preserve their privacy.) reported issues with connecting to Matrix over port 8448, probably due to a firewall, so I decided to run the webserver on the RockPro64 as well, making Matrix accessible on port 443 alongside port 8448. Indeed, all of it runs on NixOS, the configs of which also are public, to be found in a Git repo linked on the source code page. The old AMD64 server has been shut down.
### Pictures
![A RockPro64 with a passive heatsink and a Kingston A2000 SSD in an acrylic case](rockpro64.webp)
I got the idea for using longer screws with shafts in order to be able to fit the top panel while the PCIe adapter is in place from [this forum thread](https://forum.pine64.org/showthread.php?tid=7242). I'd link the explicit post, but at time of writing, there is a server side error on the site (`1021 - Disk full (/tmp/#sql_3cff_0.MAI); waiting for someone to free some space... (errno: 28 "No space left on device")`). This one runs all of the HTTP stuff (Matrix, this webserver, Matrix bots, Plik, the GTRR), with most of the relevant data being on the SSD (one partition of it is mounted at `/var/lib`, the other at `/srv`.

<ins>Update: The error has been resolved. The exact post is [here](https://forum.pine64.org/showthread.php?tid=7242&pid=45275#pid45275).</ins>

![Two Pine64 Rock64s in aluminum cases](rock64s.webp)
Left one is the 2GB one with 16GB of eMMC, running Murmur, while the right one is not plugged in currently, as it is the 4GB one with 64GBs of eMMC, running nothing.
![An Odroid HC4 with two WD RED 3TB HDDs](hc4.webp)
This one runs BorgBackup for both our own and other's clients as well as our servers and among-sus.

T.: The cables to the right are from our 3D printer.

## Services
See [/services](/services) for details on the individual services.
- The [GTRR](https://gtrr.artemislena.eu) has been made easier to use, and has gotten its own Cobalt site rather than being built as part of this site. Heads-up: This means it is not accessible via `/services/gtrr/` anymore, however, the templates and styles are public this way.
- Plik has been added.
- Element has been added. Unlike other instances, it sets a CSP for itself, increasing its resilience against XSS attacks (DeM: XSS stands for Cross Site Scripting, and is one of the most common attacks against web applications.).
- Mumble works again, and has been made a public server.
- whoareyou is up.
- The Matrix server has migrated to this domain. All users and bots seem to have managed to follow the migration.

## Bots
The JSON Feed bot now uses the aiohttp library rather than shelling out to cURL. This has mainly both happened and been possible due to the migration to NixOS. The Techlore FAQ Bot now uses an account on `techlore.net`.
