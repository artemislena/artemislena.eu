---
title: "Service additions"
data: {author: "Laura"}
published_date: 2021-09-11 21:43:45 +0200
---
Hey there, thought I'd write this one. Only one here who hasn't written anything yet, after all. It seems to get hard to get more creative titles with this sort of post, so that one's what we went with. Anyway.

## Nitter
We gave this one up for now. It doesn't work on ARM due to being written in Nim, so that's it, I guess. We might set it up on another machine and then proxy or something, dunno.

## Veloren
We have a server for that now, running on an Igel M340C we bought used (with a different SSD in it than the stock one). Veloren needs a fast CPU, so we couldn't just use our Rock64 or so.

## CryptPad
Has been running for a while, but seems like we haven't posted on that yet. It's on [pad.artemislena.eu](https://pad.artemislena.eu).

## Piped (or another YouTube frontend)
T.: Coming soon™ ^^

## Evertrue modpack
[Evertrue](https://evertrue.neocities.org) has its own download for an updated version of that now, so we removed our download link for it.

And, yeah, that's it. Questions? (and no, I'm not in Bloggers Gathering atm)
