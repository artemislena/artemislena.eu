---
title: "Matrix is weird"
published_date: 2021-01-28 11:13:28 +0100
data: {author: "FantasyCookie17"}
---
I still have no idea what actually was going on… Most of what I know is in [this issue](https://github.com/matrix-org/synapse/issues/9185). Basically, my server was sending events successfully, yet, they did not show up on other servers, and then today, it suddenly worked again for whatever reason…

In other news, `fantasycookie17.onederfultech.com` now redirects to `fantasycookie17.cf`, and I might get a "proper", paid domain during the next Netcup (my favourite registrar so far) sale, probably fantasycookie17.eu. A friend even offered to pay for it if I could not, however, given how cheap these actually are, I might just get it myself then. Good thing about Netcup sales is you actually get a permanent price reduction even after the sale is over if you register during it, from what I have heard. While I will try to keep up the onederfultech.com domain, and, once I have the .eu domain, the .cf domain, I can only advise you to always link to the most recent domain whenever possible, as I cannot make any promises as for how long I can keep the others. Sorry for any inconveniences caused.
