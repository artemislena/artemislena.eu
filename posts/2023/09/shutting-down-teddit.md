---
title: Shutting down Teddit
data: {author: Tanith}
published_date: 2023-09-15 23:39:35 +0200
---

So, [as mentioned earlier](/posts/2023/06/reddit-api.html), Reddit made API changes, which had an impact on both Teddit n Libreddit. Teddit now states on [its repo](https://codeberg.org/teddit/teddit) it's not actively maintained anymore. We figured this prolly means there won't be library updates etc. anymore, meaning we'll run into the 0-days becoming n-days problem eventually; i.e. security vulnerabilities found not being patched on time. Don't wanna afford that risk, so we decided shutting Teddit down.

As for Libreddit, well, [there's attempts on dealing better w the ratelimits](https://github.com/libreddit/libreddit/issues/836). Until that goes anywhere, we'll be using a friend's Libreddit fork which uses random Tor circuits for all requests, which helps getting around the ratelimiting, too. That's, 'f't goes anywhere, anyway – it's entirely possible Libreddit's gonna die, too, in which case we'll shut that down as well. After which, there won't be any Reddit frontends hosted by us anymore – but that's ok, seems like Reddit's dying, anyway. (same goes for Twitter/X, as for which, Nitter seems broken too? But t'least Nitter's working on that, tho that doesn't necessarily mean we won't shut that down either)

In the long term, I'm guessing we're all better off w more open platforms, like Fedi – these frontends're just band-aids at best, really.
