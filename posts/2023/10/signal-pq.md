---
title: Signal got post-quantum crypto
data: {author: Tanith}
published_date: 2023-10-08 21:26:20 +0200
---
Last month, Signal published [a post stating they had post-quantum cryptography now](https://signal.org/blog/pqxdh/). For the less technical people, I'd recommend reading that post as't starts w the basics n stuff.

For those already knowledgeable on cryptography, here's the TLDR: Signal combined their existing useagea Ed25519 w CRYSTALS-Kyber, so both needa be cracked for decrypting chats. This's important as't prevents HNDL (Harvest now, decrypt later) attacks (assuming CRYSTALS-Kyber's actually safe, anyway). Note that currently open chats're not using the new crypto, only newly initiated ones will, n only'f both clients're up-to-date. Eventually, existing chats're gonna be migrated, tho, n the post-quantum crypto'll become required for new chats.

It's kinda an exciting change, I mean, so far the only mainstream software projects that offered post-quantum stuff, as far as I'm aware, were Wireguard (via symmetric pre-shared keys, only) n OpenSSH (via NTRU Prime + Ed25519 key exchange, since OpenSSH 9.0, note that NTRU Prime's not the main NIST recommendation currently, [that being CRYSTALS-Kyber](https://csrc.nist.gov/Projects/post-quantum-cryptography/selected-algorithms-2022)), n botha these seem more relevant for like, the IT crowd, whereas Signal's an app w a more diverse userbase, I guess. Let's hope, assuming quantum computers which can crack current asymmetric crypto become a thing, even more widely used protocols adopt post-quantum standards, such as TLS (at which point, once websites start using the new TLS version, ya should prolly change all ur passwords?).

In other news, we've been thinking bout setting up an [Owncast](https://owncast.online) instance, likely on our VPS (for bandwidth reasons), n livestream stuff like us playing games on't. Among our readers, 're there any interested in watching that? Feel free [contacting us](/contact.html) via the usual channels. I also [posted a poll on Fedi](https://plural.cafe/@tanith/111199206906458916) 'f ya'd like responding there (unfortunately, opening the link in ur browser while not logged in doesn't appeara do anything useful, ya may needa access't from a Fedi client where ur logged in).
