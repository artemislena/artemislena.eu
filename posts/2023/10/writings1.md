---
title: Writings 1
data: {author: Laura}
published_date: 2023-10-08 21:40:45 +0200
---
Someone on Fedi gave people the following writing prompt:

> A magic potion turns up in the mail, for some reason, against all that makes sense, you are compelled to drink it. Immediately your body starts to change, transforming into the body that will make you happiest.

I uhm, had had a writing idea for a while that ultimately could be changed to fit that prompt, and uh, them asking people to write on it did motivate me to do so. So uh, here we go: [Link to the story](/writings/magic-potion.html) No particular content warnings for this one, I guess, apart from things inherent to the prompt. There uh, is one mention of drugs in it, and uh, psychosis as well, but only in exclusion, rather than as a description of an actual experience. Nothing lewd/sexual, no violence, nothing like that.
