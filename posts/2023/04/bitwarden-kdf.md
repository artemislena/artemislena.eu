---
title: Bitwarden KDF ‒ important security advisory
published_date: 2023-04-04 12:13:58 +0200
data: {author: Tanith}
---

So, we recently found the blog ["Almost Secure"](https://palant.info) while looking for details on how encryption is done in the KeePass format. It's got a lotta nice posts, including [one that details possible security issues with Bitwarden](https://palant.info/2023/01/23/bitwarden-design-flaw-server-side-iterations/). Since we often recommend Bitwarden when asked for password managers (we use KeePassXC ourselves but definitely can't recommend that for everyone), I thought it'd make sense warning people bout possible issues, n how ya can fix them. Read the linked post'f ya want more details on the technical background.

## The short version (for people who know what we're talking bout)
Set the KDFa Argon2 in the web interface's security settings, default settings might be fine, but ya can increase them'f ya want (for example, KeePassXC uses more like 10 rounds n 8 threads by default). 'f ur using Vaultwarden, make sure it's upgradeda [version 1.28.0](https://github.com/dani-garcia/vaultwarden/releases/tag/1.28.0) or higher first. For a plain Bitwarden server, make sure it's [version 2023.2.0](https://github.com/bitwarden/server/releases/tag/v2023.2.0) or later. Make sure clients're version 2023.2.0 or higher, too.

## The long version (especially useful for less technically educated people)

### What's a KDF?
"KDF"'s short for "key derivation function". Ya might've noticed passwords can be more or less any length, yet, for encryption, a fixed-size key's used (usually 256 bits). When ya unlock ur Bitwarden vault (or anything else that's encrypted w a passphrase), what happens in the background (since we got that question, yes, without further user interaction) 's that the app uses that key derivation function for transforming the password into the encryption key, which's then used for decrypting the vault.

### What's the problem?
Bitwarden, by default, uses a KDF called PBKDF2. It's by now a very old KDF that runs very quickly on modern hardware (which's a problem because that means attackers can guess lotsa passphrases per second, which means weaker passwords could possibly be brute-forced). However, 't also supports Argon2 since very recently. Argon2 was designed w modern hardware in mind n's much slower (tho, not so slow ur gonna notice a delay unless ur tryna guess thousandsa passwords a second) n hardera parallelize (which's good, since't limits the speed w which passphrases could be guessed).

### So, whatta do?
Note: Button labels n such mentioned here may differ depending on language settings.
1. Update all ur Bitwarden apps/browser extensions to the latest version, 'f they're not already (as for which, plz always keep all ur apps up to date, especially the security-critical ones).
1. Log into the Bitwarden web interface ('f ur using the default Bitwarden server ('f ya don't know what a Bitwarden server's, most likely y'are), that's at [vault.bitwarden.com](https://vault.bitwarden.com))
1. Navigatea Account Settings, from there "Security", n click "Keys" there.
1. There should be a field sayin "PKDF2". Click that, n select "Argon2id" there instead. 'f it's not available, n ur not using the default Bitwarden server, contact ur server admin n tell them they should update. See the "short version" above'f they ask for version numbers (or just link them this post right away). Start again at step 2 once they've updated. [Bitwarden themselves also got a tiny guide for this step](https://bitwarden.com/help/kdf-algorithms/#changing-kdf-algorithm).
1. Click "change KDF" n confirm any further prompts.
1. Ur prolly gonna get logged outta Bitwarden apps n browser extensions now, within roughly an hour. 'f ur not, n they're behaving weirdly, log out manually. Then, just re-login as usual (enter email address n password as well as any 2FA stuff'f that's configured, that's it).
