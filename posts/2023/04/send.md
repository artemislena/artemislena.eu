---
title: "Send (the service's literally called that, I didn't name this post generically on purpose)"
data: {author: Tanith}
published_date: 2023-04-13 21:08:22 +0200
---
We were scrolling thru [PrivacyGuides](https://privacyguides.org) recommendations again recently (for not being so much into privacy anymore, we've been there an awful lot recently). Noticed that the file sharing category was recommending [Send](https://github.com/timvisee/send) (it's an actively maintained forka the now-deprecated Firefox Send, w branding removed) rather than Plik, cause, well, Plik has no end-to-end encryption; files're stored in plaintext on server. Meanwhile, Send encrypts uploads w AES-128-GCM n encodes the key into the URL. Ofc, 'f the server's compromised n madea serve malicious JavaScript, things could still be read when uploading or downloading, but t'least stuff's encrypted at rest. Kinda the same threat model as, say, CryptPad. Or ProtonMail Ig.

So, what we did, we [set up a Send instance](https://send.artemislena.eu), n marked the Plik instance as deprecated ‒ web UI has been disabled n replaced w a notice, download links're gonna continue working until we finally shut things down, once all the uploads've expired. 've fun trying out Send, 'f ya want ^^

In other news, we now installed [SimpleX Chat](https://simplex.chat). Supposedly, 't provides better anonymity than other messengers, so'f ya wanna message us w "maximum privacy", there ya go. There's a link n a QR code for contacting us on [our contact page](/contact.html).
