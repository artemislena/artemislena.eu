---
title: Reddit API changes
data: {author: Tanith}
published_date: 2023-06-29 10:26:56 +0200
---

So, as y'all might've heard, Reddit's planning on changing their API terms in a way that'd break most non-commercial third party apps. This likely means we gotta shut down Libreddit n Teddit soon, as they'll stop working (specifically, on July 1st, as far as we can tell).

For the future, well, [Libreddit seemsa be planning sth](https://github.com/libreddit/libreddit/issues/785), possibly? Teddit we're not sure, [they might wanna look into web scraping](https://codeberg.org/teddit/teddit/issues/400), we'll see how feasible that's (might end up running into ratelimits w that). 'f either circumvents the new restrictions in a sensible way, we might start hosting them again. In the meantime, [RedReader](https://github.com/QuantumBadger/RedReader) 's a free app that's still gonna work – they got an exception as they're noncommercial n focused on accessibility, however, 'f Reddit improves their own accessibility, they might as well revoke that exception, sadly.
