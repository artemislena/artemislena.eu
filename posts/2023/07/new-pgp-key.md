---
title: "New PGP key (I hate PGP/GPG)"
published_date: 2023-07-11 19:10:55 +0200
data: {author: Tanith}
---

Recently got several PGP encrypted emails we couldn't decrypt. First time we thought the issue was on the sender's side perhaps, cause we didn't seema recognize the key't was encrypted w, but turns out the issue might've been on our side after all. When we got our second YubiKey (as a backup), we wanteda put our PGP keys on there, but somehow GPG only imported the main key from our backup, not the subkeys (I think we first gotta delete everything in `~/.gnupg/private-keys-v1.d/` so't doesn't remember the subkeys still being on the smartcard), so we hada make new subkeys, didn't think that'd be an issue. Turns out't was, I'm guessing the new encryption subkey was preferred for encryption n't didn't make a second copy for the old one when people sent emails, so we couldn't decrypt unless we used the backup subkey. *So* annoying.

Well, re-setting everything up was really annoying too (hada change SSH keys n signature keys for all machines n Git forges, make a paper backup, redo the Keyoxide proofs, etc.), but at the very least people can send us encrypted mail now that we can actually read ('f they insist on sending PGP-encrypted mail, anyway). But, this'd never happened'f PGP wasn't so confusing w subkeys n stuff, or'f GPG had a more clear UX for working around that. Our new key got the ID `7973D087497ED49AF3DCD66E412A96FD1D74214B`, it's on the WKD n on [keys.openpgp.org](https://keys.openpgp.org), n it's signed by our old key. Plz don't use our old key anymore for anything (except signatures made priora today). 'f ya gotta verify stuff, it's also Minisign-signed on [our verify page](/services/verify.html).

Note: We've no reasona believe our old key was compromised, 't just was a bit messy, that's all.

In nicer news, Libreddit n Teddit're still working ‒ but ofc there's the risk they'll be blocked'f they generate too much traffic, so we'd really recommend using RedReader on Android (or other alternative clients that still work, not that I expect there's many). Also, we're planning on being at CSD again this year [like last year](https://artemislena.eu/posts/2022/07/csd-rostock/), in Berlin ofc this time; it's on July 22nd. Going there too? Feel freea say hi'f ya see us, or even ask us beforehand'f ya wanna meet up somewhere specifically ^^
