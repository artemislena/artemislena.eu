---
title: CSD Berlin 2023
data: {author: Tanith}
published_date: 2023-07-22 23:38:20 +0200
permalink: "/{{parent}}/"
---
Like [last year](/posts/2022/07/csd-rostock-2022/), we were at CSD again, but since we moved, not in Rostock, but in Berlin ^^ Proof:
![Artemis at CSD, holding a pride flag and wearing one as a cape](artemis.webp)
Ofc, CSD's best spent together w the people ya love, right? Such as the magnificent n absolutely lovely [Anna Aurora](https://annaaurora.eu) (check out her blog, she'll also do a post on CSD):
![Artemis and Anna, hugging while wearing pride flag capes](girlfriends.webp)
One thing we noticed right away, unfortunately there was quite a corporate presence there, unlike in Rostock; mosta the trucks were just ads for various companies. Also there were more cops (maybe needed for clearing the streets? unsure).
![Lots of rainbow people in front of various trucks with ads on them](trucks.webp)
Sure, companies supporting us's nice, but for the most part't feels like they're just doing't for gaining profit, yk. Whatever. Mosta these trucks also seemeda play very loud music, so we rather sped past them ‒ in fact, at several points we ended up in fronta the entire main parade (not that we were the only ones):
![Front of the parade, people are carrying an oversized rainbow flag, and a banner saying "Wir sind hier nicht zum Spaß!"](front.webp)
On the other hand, there was more like, yk, political stuff going on than there was at Rostock. (the banner says, in German, <q>We're not here just for fun!</q>)

L: Political *stuff*?

<details><summary>Content warning: Queermisia, death penalty</summary>
Well, there was someone there from Namibia, who talked bout how in Africa, several countries either had made or were in the processa making new laws against queer people, to the pointa genocide (not that they used that term, but'f there's a death penalty for belonginga some group, Idk what't is'f not genocide). So, while we're out there, having fun waving our flags, there's people elsewhere fearing they'll goa jail, or worse. Not great, but. Maybe that makes't even more important, we're out, waving our flags. We're here n we're queer. They appealeda us, we in Europe gotta hold our governments accountable ‒ for laws they're releasing here, but also for deals they may be making w countries which release such laws. Let's not finance the hate, Ig? CSD is n, sadly, currently has to be, a protest. Maybe'f things go further in this direction, 't might needa be a riot again. Maybe that's already the case, that's beyond our judgement really. But queer rights're human rights, n I think our readers can agree human rights're worth fighting for, 'f needsa be. We're not intent on destroying "them" (as in, the institutions n people responsible for queermisia), but they're intent on eradicating us. That's a fundamental imbalance, I s'pose? Maybe'f only they could see that, the world might be a better place. Or maybe they don't even care, who knows. I don't, I'm just a pretentious fuck writing from my relatively safe ivory tower here in Germany (not that Germany's gonna be safe for long either, 'f all that stuff from, say, the US, properly takes foot here). Dunno, should prolly stop here. Things got a tendency for going wrong whenever I start talking politics.</details>

Oh, n last but not least, we thought this cloud looked cool.
![A cool cloud](coolcloud.webp)

<ins datetime="2023-07-23 02:02:30+0200">Update 2023-07-23 02:02:30 +0200: Forgot including the picturea the fronta the parade n the text commenting on't, added that now.</ins>
