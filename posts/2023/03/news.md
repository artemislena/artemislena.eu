---
title: "March 2023 news"
data: {author: "Laura and Tanith"}
published_date: 2023-03-19 23:52:30 +0100
---

L: So uh, we didn't quite know what to post about this month

T.: Shush. Don't listena *her*. We had so many ideas but they were all so boring we decided on this exciting new thing!

L: Like what ideas?

T.: I just said the other stuff was boring.

L: Uhm, I'd still like to know what other ideas we're supposed to have had.

T.: … Anyway. So, we do catch some news here n there, n we thought maybe we'd share somea the perhaps lesser-known stuff. Yk, all the big news ya can get on larger news outlets, we're mainly focusing on random tech news n stuff we heard bout ‒ nothing
*too* important, perhaps. Don't really wanna comment on politics too much rn anyway (there's some good commentators out there, fwiw, but like, no matter what, ur gonna be subjecta controversy at some point so we rather stay outta that), tho that may
change in the future, dunno.

L: Uh, I'm still curious about

T.: 're ya gonna stop asking'f I bribe ya w a hug?

L: I mean, uh, you usually give these out for free anyway, right?

T.: Yeah, but not this time.

L: Alright then.

T.: *hugs*

L: *hugs back*

## Docker Hub discontinues its free tier for teams
L: So, [according to this toot](https://hacky.town/@rtyler/110023261891172425), users of "Free Teams" on <a href="https://hub.docker.com" rel="nofollow">Docker Hub</a> will have their accounts suspended next month unless they upgrade; they got an email saying so last month. We also tried getting primary sources on this, since we uh, didn't get that email (as we don't use Docker Hub). Our first idea was to uh, check out [the Wayback Machine](https://archive.org), but turns out there Free Teams must have been discontinued more than a year ago, as on January 1 2022 there still wasn't any Free Team tier on their pricing page, much less more recently (so, they probably did continue these "legacy accounts" for a long time). Anyway, a comment elsewhere noted <a href="https://www.docker.com/blog/we-apologize-we-did-a-terrible-job-announcing-the-end-of-docker-free-teams/" rel="nofollow">they wrote a post on their corporate blog apologizing for confusion around the announcement</a>. There, they suggested you could just <a href="https://www.docker.com/community/open-source/application/" rel="nofollow">apply to their open source program instead</a>, which uh, would give you equivalent or even better features, but uhm, that requires writing an application. So, heads-up, if you're using images from Docker Hub provided by an "organization" rather than a "user", you might want to check if they're switching to another platform soon, or uh, maybe starting to pay (or even apply to that open source program, who knows).

## "Acropalypse" vulnerability
T.: There's <a href="https://twitter.com/itssimontime/status/1636857478263750656" rel ="nofollow">this tweet</a> claiming that the screenshot editor on Pixels' stock OS, Markup, apparently leaves data from the entire screenshot somewhere even after it's cropped or redacted, which, well, 's a pretty serious privacy issue. Dunno whether it's been fixed, but for affected screenshots, there's [a demo site](https://acropalypse.app/). We tested w screenshots taken on our GrapheneOS Pixel n couldn't get't working (in fact, the demo expects PNGs whereas Graphene saves em as JPEGs), n [this blogpost](https://www.da.vidbuchanan.co.uk/blog/exploiting-acropalypse.html) mentions

> The bug lies in closed-source Google-proprietary code

which means that this should in fact only affect the stock OS n not alternative Android OSes.
