---
title: "Review: Studded brick plants"
published_date: 2023-05-31 21:13:15 +0200
permalink: /{{parent}}
data: {author: Tanith}
---
Recently, we were at a girlfriend's place n noticed she had some flowers n other plants by Lego there. We thought they were quite pretty

fc17: Pwetty flowers!

Cutie ^^ … but Lego's known for being kinda expensive compareda other studded brick brands. So we had a look on [BlueBrixx](https://bluebrixx.com), a German store offering both their own linea studded brick sets as well as sets by various brands (we think they're all Chinese but not sure, not like't should matter a lot). Turns out, they were *a lot* cheaper (t'least that's we felt, we didn't use objective measures like brick count n rather just looked at roughly comparable sets, admittedly the Lego ones were a bit larger n maybe looked a bit nicer, but overall not worth the price delta, we'd say).

So we ordered several things from BlueBrixx (not linking them cause there's no way we can get language-unspecific links from BlueBrixx as far as we can tell):
- red orchids by Jaki, standard-size bricks
- white orchids by Zhe Gao, mini bricks
- seven succulents, set 3 by Qi Zhi Le (brand's labeled as "others" on the website, that's just what the package says), standard bricks
- two cacti in animal pots (lion n cat) by MoYu, diamond blocks

After a long weekend, we've finished building them all, n figured we'd review them, so other people lookinga decorate their space may know whatta expect from non-Lego brands.

## Some general remarks
Feels like we haven't built Lego or other studded brick stuff in ages. Totally forgot how pressing all those bricks together makes ur fingers hurt after a while ‒ the smaller n pointier they're, the worse. N brick separators're a great fingernail saver ‒ tho only as long as they work, 'f the gaps're too tight, ya gotta use ur fingernails eventually, n then ur gonna get dents n sometimes even cracks in them sooner or later. One thing we noticed w several sets ‒ often, there's spare parts, but it's not always cleara which kind there's gonna be spare parts, n how manya which kind're expecteda be left. Dear manufacturers (not that I assume ur gonna be reading this but it's worth a try), would't be possible putting a lista parts which should be left over in the end somewhere? Would make't easier telling'f there's any step ya forgot.

For better or for worse, mosta the plants (except the seven succulents) had their "soil" made by just loosely putting a large amounta tiny bricks (round 1x1 tiles, usually, Lego does this too w their larger plants, tho they use larger bricks for that). 't did make for a unique look, but ofc these small parts're easily lost, n usually the instructions had ya putting the stuff in before the final steps, so't was quite possible some fell out again later on.

## Red orchids
![Red phalaenopsis orchids in a grey pot, all made from studded bricks](./red_orchids.webp)

I'd say these were pretty nice. They came w a brick separator, the instructions were easya follow (not too much happening at once, n previous steps were whited out), n the end result's very pretty; from a distance they almost look like real orchids. No real complaints here, besides that the shield parts used for somea the petals had a handle that was a bit thin on one side, making them harda be clipped on in a stable way.

## White orchids
![White phalaenopsis orchids in a blue pot w white decorations, made from studded bricks](./white_orchids.webp)
These were kinda a bit disappointing. We liked the box, twas less a throwaway box n morea like those boxes ya get w puzzles. Gonna keep that one. There also was a brick separator here, specifically made for those mini bricks. The instructions were not nice. Previous steps weren't greyed or whited out, they just highlighted newly added stuff w a red outline that sometimes was very poorly visible. Speakinga poorly visible, the lines between the bricks for the pot were, as't's dark blue n they drew the lines in black, making for an incredibly poor contrast. We ended up w several bricks that didn't seem like they should be spare parts, given their size, but given the pot seemed symmetric on the outside, we couldn't figure whether anything was missing. They didn'tve numbered bags tho (albeit only two numbers, one for the pot n soil, the other for the plants inside't). There was the same problem w clipping on the petals as Jaki's orchids did, but also, they used clips for some tentacle-like pieces (used for some green leaf stuff or whatever) that didn't fit for those pieces at all (ya can't see on the pic, but they're tilted upwards a bit). They also ended up quite a bit smaller than the Jaki orchids ‒ but that'sa be expected at the lower price n smaller bricks. We did like how they put work into making a decorated pot n covering up the holes in the technic partsa the flower stem. Overall, we like the looka the Jaki orchids more tho, somehow.

## Seven succulents
![Four small potted succulents, made from studded bricks](./succulents.webp)
We only put foura the seven we got on this pic, i.e. there's three we didn't photograph, but I don't think that should be an issue? There's 28 different ones in total, in BlueBrixx's physical stores they're sold individually as surprise boxes ('f ya wanna calculate the chance for getting the same twice, use the math behind the birthday problem, 'f ya wanna get any in particular, well, it's a binomial distribution), but online, they sell them in four different setsa seven. We really liked how that one blue one looked, so we went w set 3, but the other sets looked nice too. Initially, these were pretty enjoyable. Easya build in just a couple minutes each. However, the bricks weren't great quality ‒ some just didn't hold well. Also, w some, like that green one made mainly from gears, stuff just didn't fit like't should; things were too crammed. So, dunno. Ya get seven tiny plants for a low price, n they're okay overall, n for the most part quite fun builds (the fact they were so quickly built made't a rather rewarding experience), Ig.

## Lion n cat cacti
![Two potted cacti, one pot lion-shaped, the other cat-shaped, made from very tiny studded bricks](./feline_cacti.webp)
fc17: Lion lickers!

*Steven Universe* might've been a reason we opted for the lion specifically, even'f, say, the penguin was cute too ^^ Anyway, oh my gosh these were a paina build. They tried keeping the instructions short by adding two layers in every step, which resulted in us missing one seemingly every couple steps -_- We then only noticed a long time later n always hada take several things apart for fixing our mistakes, as for which, the brick separators they came w didn't even've a thin end, so we ended up using the thin enda the brick separator for the mini bricks (which didn't always work, sadly). Also, w the "diamond blocks" being very tiny, but harda push on, these were prolly the ones that made our fingers hurt the most. 'f ya end up building these, really always pick out the parts for each step first, before ya start building that step, n then make sure ya use up every single brick. It'll save ya a lotta hassle later on. Still, we think't was worth't. The "pot shaped like a cute animal face" 's sth we haven't seen anywhere else yet, n like, they really *are* cute. Even'f stuff like the bricks the cat's whiskers were put on or the lion's mane didn't fit perfectly, we'd say they do look nice, overall.

N that's't. Haven't written such a long post in a while, then again a lotta previous posts felt way too short. Hope ya enjoyed't ^^ 're ya thinking bout getting brick-based plants or other brick-based decorations? What're ur experiences w these brands?
Feel freea let us know by [our usual meansa contact](/contact.html).
