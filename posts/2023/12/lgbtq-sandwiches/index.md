---
title: "Cooking with Tanny: LGBTQ sandwiches"
data: {author: "Tanith", cw: "(Vegan) food"}
permalink: /{{parent}}/
published_date: 2023-12-22 23:10:25 +0200
---
Happy winter solstice everyone ^^ Also, we implemented content warnings on our blog (finally). Let us know'f they're accessible or not, n'f ya think their UX could be improved somehow. Oh, n we switcheda our own Fedi instance (rather than using [plural.cafe](https://plural.cafe)). Alas, neither's y I'm writing this post today.

So, ya may've hearda LGBT sandwiches. They're based on the classic BLT (bacon, lettuce, tomatoes) sandwich, but add guacamole, n ya end up w a nice acronym 🏳️‍🌈 Ofc, ya can further expand on that by adding a "Q". We thought "quinoa" at first, but then looked online, n saw that people used "queso" (cheese). So, we figured there'd be some waya make a vegan versiona this.

Ur gonna need some sorta sandwich bread, vegan bacon, vegan meltable cheese, lettuce, tomatoes, one avocado, n some spices (e.g. paprika powder and chili flakes work well).

1. Turn the oven on a low temperature (sth like 100ºC maybe). Put the vegan cheese on the bread slices, put them in the oven, leave them there until the bread's toasted n the cheese's molten.
1. In the meantime, fry the vegan bacon in a pan w some oil. Note it's likely gonna be quite floppy while it's still in the pan, but it's gonna get crispy as ya let't cool off.
1. Cut the tomatoes into slices, n take leaves from the lettuce-head.
1. For the guacamole, take the peel off the avocado, remove the seed from the center, n cut the pulp into pieces. Mush the pieces in a bowl, n add spices as ya like.
1. Once the bread w the cheese's done, put somea every kinda the ingredients ya just prepared on one slice, n then put another slice on top. Tada, ya made a sandwich ^^

N that's what't looks like (not too fancy, Ik, the spices made the guacamole more brown than green, but I swear't was tasty):

![A sandwich w nicely toasted bread, crispy vegan bacon, guacamole, tomatoes, n a tiny bita lettuce](sandwich.webp)
