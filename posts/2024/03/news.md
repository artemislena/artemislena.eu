---
title: March 2024 security and privacy news
data: {author: Dea ex Machina}
published_date: 2024-03-31 23:33:20 +0200
---
This month, several things within the wider field of privacy and security happened that we felt we should report on.

## Signal introducing usernames
[Signal allows for contacting others using usernames now](https://signal.org/blog/phone-number-privacy-usernames/) (note their blog post was written last month, however the change was not rolled out on the stable version before this month, I believe). This means that you can now chat with other beings on Signal without revealing your phone number to them, and also, we will be able to allow for us being contacted directly on Signal ‒ however, we have been told that for the time being, for accounts created before this change, it might still be possible to bruteforce their phone number, if the public key is known (e.g. due to knowing their user ID), so we will not publish it just yet, hence why we have not updated [the contact page](/contact.html) at time of writing. We will do so once we believe it is safe. Do note that, while you do not need to reveal your phone number to others anymore, a phone number is still required to sign up to Signal.

## SimpleX post-quantum cryptography
[SimpleX optionally supports post-quantum key exchanges since version 5.6](https://simplex.chat/blog/20240323-simplex-network-privacy-non-profit-v5-6-quantum-resistant-e2e-encryption-simple-migration.html#quantum-resistant-end-to-end-encryption-beta). They appear to believe that, the way they are doing it, it is more secure than [Signal's implementation](/posts/2023/10/signal-pq.html), as (according to their claims, unlike Signal) their implementation does not break the double ratchet's self-healing properties. However, they also base their choice of KEM (sntrup761 rather than Kyber) on <a rel="nofollow" href="https://blog.cr.yp.to/20231003-countcorrectly.html">djb's post</a>, whose line of argumentation has been criticised, particularly the claim that NIST calculated the attack costs on Kyber wrongly (although to our knowledge there is at least nothing wrong with the algorithm SimpleX picked, either, it is used by OpenSSH after all, whom we trust to make sensible choices in cryptography).

## XZ backdoor
Apparently, someone spent years gaining the trust of the maintainer of the XZ compression tool, in order to insert a backdoor into versions 5.6.0 and 5.6.1. It seems that this backdoor would only be inserted into dpkg and RPM builds on x86_64 Linux, and that it somehow alters the execution of sshd when it is linked to liblzma (which again depends on XZ), however, it is as of now unclear if the backdoor also applies to other things linked to liblzma, and what exactly it would do, anyway. [Evan Boehs' post provides more insight on the matter than we could do here.](https://boehs.org/node/everything-i-know-about-the-xz-backdoor)
