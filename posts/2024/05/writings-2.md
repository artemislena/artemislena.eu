---
title: Writings 2
data: {author: Laura}
published_date: 2024-05-18 19:44:05 +0200
---

So uh, earlier this month, we had this dream. It was mostly inspired by a <i>Doctor Who</i> episode we had watched, and also probably by <i>Barotrauma</i>, but uh, ultimately, it was its own thing, with a very unique world, we felt.

We realized we could probably turn it into a somewhat decent short story if we added some more details, which we did. And after getting some (overall positive) feedback from friends, decided to publish it.

Content warnings for abusive governments, unethical research, police violence, one mention of death, mild horror/a threatening atmosphere, and ‒ as might be expected from a dream ‒ overall powerlessness of the protagonists. [Link to the story](/writings/underwater-lighthouse.html). Enjoy! Please don't try to read too much into it in terms of uh, any sort of moral arguments we might be making; the general plot structure *is* copied from a dream after all.
