---
title: Absence note
data: {author: Tanith}
published_date: 2024-02-29 21:25:14 +0100
---
We're up for another hospital stay next month, so we won't be available for some time for anything requiring our physical presence – i.e. just like last time, 'f there's any kernels/bootloaders/hardware/SSH servers breaking, we can't fix't. We'll still've remote access in any other case n should be ablea fix anything on a higher level, assuming we'llve enough energy for that as we're recovering from surgery (at the very least we might take a bit longer w respondinga any admin-related requests).

Can't say yet for how long we'll be gone exactly. We're leaving tomorrow (on March 1st), n we'll definitely be gone t'least until the enda next week, but possibly up to a week more than that.
