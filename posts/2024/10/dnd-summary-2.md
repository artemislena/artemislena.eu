---
title: DnD summary, part 2
data: {author: Laura}
published_date: 2024-10-20 16:44:30 +0200
---
Hey, here goes our next post in this series. Now that we're done with writing the initial events of the first four or so sessions, we'll uh, try to post more regularly, ideally one post per session, so they don't get quite as long and maybe they'll be a bit easier to read for those interested. As previously, all events described are fictional, and so on.

This blog post is unofficial Fan Content permitted under the Fan Content Policy. Not approved/endorsed by Wizards. Portions of the materials used are property of Wizards of the Coast. ©Wizards of the Coast LLC.

## Flowers and gems
Being elves, we didn't need to sleep at night, just spend a few hours in trance ‒ a bit longer for Cookie, since she's still a child. So, I decided to stay with her in our room until she was fully awake again, and Dea and Tanith went to the magic item store. Tanith wasn't quite happy with her crystal after all, she decided she wanted a wand instead. Not one of those boring wooden ones, but a metal one, with a gem tip. Dea also thought xe could sell one of xyr scrolls that xe had copied into xyr spellbook, now.

When they returned (it took a bit, since the store wasn't open yet), Tanith had exchanged her crystal for a brass wand with a blue gem tip, and Dea was indeed able to sell the scroll. Cookie and me were already eating breakfast (some nice cereals with oatmilk), and the two of them joined us in that. Just a bit later, Alice came down the stairs, too. No trace of Rob, though.

As we were all sitting together, Ixia joined us. She said teleportation wouldn't be possible, but the merchant at the magic store could make some invisibility potions for us. So, the six of us headed there.

The merchant said they were lacking the ingredients for the potions ‒ some gems and flowers ‒ but there should be some in the area. They couldn't quite say where, but they could give us a description of what they looked like. We headed out again, and, trying to recall information about these things, and asking the locals for help, we could sort of figure out where we might find some in the area. Which, then, we did, using our skills at searching, interacting with animals, and carefully harvesting them, we found quite a few of both.

## Foxes
On the way back, Alice seemed to grow visibly stressed, and was suddenly worrying that we would hate her. I tried to talk to her calmly to figure out what was up, but before our conversation really got anywhere, she dropped to the ground, and started to transform into a fox! As she did, all her equipment seemed to merge into her body.

It reminded us a bit of lycanthropy, but Cookie was convinced Alice might be a [foxkin](https://kyaas-dungeon.love/posts/brew-foxkin/) (author's note: an additional note the author of the linked homebrew description forgot, but that we did include in our campaign, is that uh, to counterbalance what would otherwise be an unlimited unrestricted wildshape, foxkin can only stay in their humanoid form for 6 hours a day), one of the fey creatures she had heard about in the fairytales we would sometimes read to her. Now, that didn't seem terribly likely, since those were just fairytales after all, but then again, it was early afternoon, there wasn't any full moon out, so lycanthropy didn't seem fitting, either ‒ especially given that werefoxes aren't exactly common, compared to e.g. werewolves.

So, I crouched down, stretching out my hand and trying to calm her down by talking to her in a calm voice. She didn't seem to be able to respond, so Cookie started casting a spell that would let her talk with animals, and Dea tried to detect if any magic was affecting her (apparently not, unlike yesterday). When Cookie finished her spell, they were indeed able to have a conversation.

Apparently, Cookie was right, she *was* a foxkin. She seemed scared at first, thinking we would kill her for her fur (it was quite unusual for a fox's; white with black and orange-ish spots). But we reassured her, we're vegan, and wouldn't want her to get hurt. Cookie also let her play with her pixie plushie for a bit, and noted she looked floofy. Now, the question was what to do, we offered she could come with us (poachers might go after a lone fox, but not one guarded by three armed adventurers (and a kid who was carrying a lyre)), and as long as she was unable to access her room key (she'd be able to transform back again tomorrow, she said), she could stay in our room at the inn.

Walking through the town, with the fox among us, didn't trigger any confrontations, although some seemed to whisper and shake their head regarding "adventurers and their weird pets". The innkeeper did ask us if the fox was intended, but otherwise didn't seem to mind much.

In our room, Alice first went under the bed, but Cookie asked her if she wanted to come up to cuddle a bit ‒ which she did. I remembered how much Alice enjoyed berries, so I conjured a bunch and let her have a couple of them (they were enchanted to keep you going for an entire day, so I figured she shouldn't eat all at once). Overall, that went pretty well, everyone seemed relaxed. Tanith and Dea brought the materials we had collected to the shop. The materials Alice collected weren't accessible to us for now, but they promised the shopkeeper they'd bring them there as soon as they could in the morning.
