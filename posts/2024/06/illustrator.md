---
title: New illustrator wanted for Be pirate, do gay
data: {author: Dea ex Machina}
published_date: 2024-06-10 16:31:15 +0200
---
[Be pirate, do gay](https://bepiratedo.gay) is a project we started together with [Anna Aurora](https://annaaurora.eu), who was the artist drawing the pages for it so far ‒ however, recently, she lost motivation in doing so, and we decided together that it might be a good idea to look for a new being who would like to take over that task.

We cannot directly pay you for anything you do, so it does help if you mainly did this for the intrinsic fun of making a webcomic together with other beings. However, if you would like to monetize any of the comic's content somehow, e.g. by setting up a virtual "tip jar", compiling several chapters into a print book, which is then sold, or selling merchandise such as t-shirts or stickers, you could keep any percentage of the earnings from that which you feel is fair (up to and including 100%) for yourself. Any name you desire would be listed in the credits, and you could also use the website and associated social media accounts to promote yourself and your work in other ways that you can come up with. Of course, we will also keep paying the registration fees for the domain, unless you would explicitly like to pay a share of it.

If you would like us to do so, we would be willing to make changes to the website; for example, we could set up an integrated comment system (rather than purely outsourcing it to the Fediverse), or change any of the design or layout, as long as it stays accessible to a preferably wide userbase.

Depending on your preference, we could keep the existing changes (likely meaning a major style change early in the comic), or replace them with pages you drew yourself. If you would like to, you could also be involved in writing the scripts for the comic, and have an influence on where the plot ends up going, although we do request that "our" characters would still be written by us.

If you are interested, [do get in touch](/contact.html). If you are hesitant, do let us know if there is anything else we could do to make this proposal more appealing to you.
