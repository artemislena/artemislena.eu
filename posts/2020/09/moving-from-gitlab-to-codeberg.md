---
title: "Moving from Gitlab to Codeberg"
published_date: 2020-09-27 13:10:09 +0200
data: {author: "Pre-plurality (likely FantasyCookie17)"}
---

I have been using [Gitlab](https://gitlab.com) for quite a while now, however, I was never too happy with it. uBlock Origin found some tracking elements on it, and it is very slow… I have been considering [Sourcehut](https://sourcehut.org), however, there are several things I do not like:
1. It may keep potential contributors away. The interface is *very* minimal and basically 
[requires you to use the `git` command line](https://cadence.moe/blog/2020-06-25-git-send-email), and the public 
[sr.ht service](https://sr.ht) is said to become paid at some point.
2. It consists of a [bunch of Python packages](https://git.sr.ht/~sircmpwn/core.sr.ht/tree/master/srht) at the core, making 
it slower and harder to selfhost than it could be.

The platform that suits me better seems to be [Gitea](https://gitea.io), especially the public [Codeberg](https://codeberg.org). I moved most of my projects from [my Gitlab account](https://gitlab.com/FantasyCookie17/) to [my Codeberg account](https://codeberg.org/FantasyCookie17/).

Of course, anyone wanting to use Sourcehut for its minimalism, or Gitlab for its many features, feel free to do this, Gitea and Codeberg are just my personal preference. Just avoid proprietary offerings such as Github or BitBucket, please. In my humble opinion, open source software should use open source development platforms as its main development channel (but of course, using a mirror on Github for better discoverability is not something I would consider wrong).
