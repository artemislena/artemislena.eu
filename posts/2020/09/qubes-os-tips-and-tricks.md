---
title: "Qubes OS: Tips and tricks"
published_date: 2020-09-19 20:46:45 +0200
data: {author: "Pre-plurality (likely FantasyCookie17)"}
---
I talked about Qubes OS in [my post on secure operating systems](/posts/2020/08/a-secure-os.html). I concluded that it is indeed quite nice, and it is also what I use, however, besides the worse performance when compared to other OSes, it also is quite buggy as a disadvantage. Some of these have the potential of making the OS unusable, and mitigations are not specified in the documentation, so I thought mentioning fixes for those here, as well as some general stuff on improving your user experience and security while using Qubes. This article assumes you are familiar with the Qubes OS documentation.

## Install process
Some things can already be improved while using the Anaconda-based installer.
### Partitioning
I recommend setting up partitioning manually after auto-generating the layout: Shrink down `/boot/efi` from the default 500MiB to something like 100MiB (this is enough), and consider using a different file system for the other partitions (apart from `swap` and `/boot/efi`).For best performance, I recommend XFS for `/` and EXT2 for `/boot`, however, other file systems may be of interest as well.

### Default templates
While I recommend having and using Whonix on Qubes for privacy reasons, you might not need Debian (or at least not the full version, just the minimal template), and not installing it will speed up the process as well as saving bandwidth and disk space.

## System not asking for passphrase while booting when using storage encryption
You need to either remove `rhgb` from or add `plymouth.ignore-serial-consoles` to the kernel boot options. The former will disable the graphical boot screen and replace it with a text interface, while the latter will make you unable to see systemd messages while booting and shutting down. Source: https://github.com/QubesOS/qubes-issues/issues/3849

## Use minimal TemplateVMs
Instrutions in the docs. I recommend this because things like Thunderbird or Firefox will take up lots of disk space while not being desired by everyone, so uninstalling the default templates (after making sure that network and such works, note there is no minimal template for Whonix) could be desired.

## Disable passwordless root in domUs
I feel like the docs understate the advantages of this. The easiest PrivEsc attack is fakesudo (basically aliasing sudo to something that grabs your password), whereas this method is immune to it, so it is great for adding a small amount of defense in depth.

## WiFi is broken
It seems like the power-saving tool `tlp` breaks certain aspects of PCIe passhtrough in default settings. Do not use it; if you have already used it, you need to reinstall your NetVM. At least it appeared to be like this for me (Intel 7265 WiFi chipset).

## Domains not booting randomly (`qrexec-agent failed to connect`)
This seems to occasionally happen. Try to boot them manually from the Qube Manager.

## Use the MirageOS-based firewall
While it necessarily requires you to trust a third party, it may increase performance and security, as well as being much more lightweight. [The Whonix wiki mentions it](https://www.whonix.org/wiki/Qubes/Why_use_Qubes_over_other_Virtualizers#Networking).
