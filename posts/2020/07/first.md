---
title: First!
published_date: 2020-07-29 13:43:55 +0200
data: {author: "Pre-plurality (likely FantasyCookie17)"}
---
For those who do not get the reference: In very active forums and news sites with comments such as [ComputerBase](https://computerbase.de) (which reminds me, I have not visited that site for probably months now, and it was not that bad after all), it kind of used to be (and probably still is) a cliché that the first person who wrote a comment would not actually write something useful, just something like <q>First!</q>, as it was not that easy to do that, considering how quickly the first comments would arrive. And, in fact, I am the first person writing something on this site… Just that this is a blog, not a forum, so, in this case, it was not even hard.

Now, I guess I am supposed to write a bit more than that. So, I guess, check out my menu, for instance? It should be in the top right corner, unless you have a weirdly sized device. Of course you do not need JavaScript to use it. I try to use standard HTML, and make my site as accessible as possible. Is anything you do not like about it? Something you would call me a "soydev" for (someone actually did that because I used to use  different headings for different styles…)? Just contact me (again, check out the menu).

## A note on dates
Now, [Cadence's first post](https://cadence.moe/blog/2020-03-13-hello-blog) said that it does not feel like the day changes until one goes to sleep and wakes up again, and therefore the post was dated one day earlier than it actually would have been. While I can agree with the first part of that on a subjective basis, I will not let the date of my posts be influenced by that, since I use the date to more or less accurately denote the time when I finished a post.

## So, what will I write here?
I guess most people kind of expect me to write something on this? Now, seriously, just read the `<h2>` on my front page. Yes, I do already have some specific posts planned, but I guess I will just write those, rather than talking about them right now.
