---
title: The source code is up
published_date: 2020-07-30 18:49:27 +0200
data: {author: "Pre-plurality (likely FantasyCookie17)"}
---
I have now uploaded all source code I wrote that I use to build this website! The corresponding link is in the menu; the document it links to also contains more info on licensing. With it, and the tools I use, you would be able to reproduce my website completely. I decided not to use a Git or Hg repo for various reasons, but really, just check it out.

## But why?
Of course, this is a static site, and there is not much that is extremely original or useful in there. Still, if you want a simple and secure setup with Caddy and Cobalt, reusing parts of it might work for you. Furthermore, the code is fully free, so really, just do whatever you want with it.

## What next?
Now, there is still some housekeeping that is remaining to be done. As the members of the Bloggers Gathering room can confirm, the Matrix RSS integration seemed to have some trouble with my server, even if I turned TLS1.2 (with secure ciphers only) on. So, I guess I will have to write an RSS bot myself… I might use Python for that, at least due to [the FAQ bot I maintain](https://gitlab.com/FantasyCookie17/techlore-faq-bot/), which uses matrix-nio, I have some slight familiarity with it, kind of. And I am rather optimistic about the existence of an RSS module for Python, perhaps even one with TLS1.3 support. I guess the hardest part will be to get it to not print the full parsed feed again once it restarts… This should not be too hard either, however. Of course, all source code will be open and free, and in this case, it will also be possible to contribute, rather than merely reporting issues. Anyway, I am also considering creating a Matrix room just for this blog, so I do not have to (ab)use Bloggers Gathering for handling comments and such. In fact, this should be done rather quickly, and might happen today. I also need to see how I can convert the `published_date` variable to [ISO8601 format](https://en.wikipedia.org/wiki/ISO_8601) using Liquid so I can set the `datetime` attribute of `<time>` correctly.
