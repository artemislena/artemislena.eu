---
title: A secure operating system
published_date: 2020-08-20 23:14:10 +0200
data: {author: "Pre-plurality (likely FantasyCookie17)"}
---
<ins>I do not use Qubes OS anymore. Consider this article outdated in various regards.</ins>

This is going to be a rather long post, and I cannot add sources for everything right away. I may add sources at any time without a notice (my edit policy still applies for other kinds of edits nevertheless). Note that for some things I might never be able to provide sources as these are simply things I heard about in public chatrooms.

As you may know, I am in the [OS Security room on Matrix](https://matrix.to/#/#os-security:artemislena.eu), in which various operating systems are discussed frequently regarding their security. The thing is, it is not that simple, as many that have reached widespread use today are much less secure than you would expect (while others which are also widely used are *more* secure than you might think, if not as secure as expected), and those that are secure basically always have some other kinds of drawbacks that are not directly related to security, but may be a dealbreaker nevertheless. I will mainly focus on desktop and mobile operating systems here, as servers have a very different quantity and quality of attack surface, and security should rather be provided by the often well-written, less complex server software, and isolation is done through hypervisors anyway, most of the time, as things like clipboard and file sharing are not that important.

## Some basic terms
It might occur to you that you are not familiar with some of the terms used here. However, I do not know how I could be better at explaining them than Wikipedia is (doing so nevertheless seems redundant), so I decided to just link their respective pages for you here. (Note: I would normally not explain many of these if they were used in a shorter blogpost without all the others, however, the sheer amount of terminology made me deem it appropriate to explain all of them.)
- [Capability-based security](https://en.wikipedia.org/wiki/Capability-based_security)
- [Microkernel](https://en.wikipedia.org/wiki/Microkernel)
- Namespace: [Wider sense](https://en.wikipedia.org/wiki/Namespace), [narrower sense](https://en.wikipedia.org/wiki/Linux_namespaces), for the latter also see [Sandboxing](https://en.wikipedia.org/wiki/Sandbox_(computer_security))
- [Formal verification](https://en.wikipedia.org/wiki/Formal_verification)
- [Control-flow Integrity (CFI)](https://en.wikipedia.org/wiki/Control-flow_integrity)
- [Address Space Layout Randomization (ASLR)](https://en.wikipedia.org/wiki/Address_space_layout_randomization)
- [Principle of least privilege](https://en.wikipedia.org/wiki/Principle_of_least_privilege)
- [Memory safety](https://en.wikipedia.org/wiki/Memory_safety)
- Verified boot: I will actually explain this one myself, because Wikipedia did not appear to have a good definition. Verified boot allows the boot firmware to prevent bootloaders and kernels that do not have a valid cryptographic signature that corresponds to some specified public key to be loaded. These are then supposed to verify the userland, effectively preventing persistent exploits. Now, there has been some criticism, because many devices only allow particular keys, however, good implementations allow you to unlock them, specify your own keys, and then relock them, meaning user freedom is not impacted by this.
- [Mandatory Access Control (MAC)](https://en.wikipedia.org/wiki/Mandatory_access_control)
- [Virtual Machine (VM)](https://en.wikipedia.org/wiki/Virtual_machine)

## Linux (or GNU/Linux)
I do not even know where to start here. Linux distros offer easy to use, fast and versatile OSes, however, the security is very lacking.
### Sandboxing
Linux systems by default have no sandboxing of applications at all, meaning a compromise of one app easily leads to full system compromise. Now, there are two "solutions" to this: The app distribution systems Snap and Flatpak. Flatpak, however, [has some quite severe issues with its sandbox](https://flatkill.org/), and while I cannot state similar things about Snap, I have been told it was not much better, and it has its own set of issues, such as only allowing a single, proprietary app store.
### General hardening and memory safety
The Linux kernel does not incorporate modern hardening techniques such as CFI, it is a monolithic kernel (which makes applying the principle of least privilege basically impossible), it is written in non-memory safe languages (mainly C), and the situation for desktop programs does not look that much better.
### GUI stack
<ins>Update: I meant the most commonly used GUI stack. Of course, there is no "default", since GNOME, as well as Wayland compositors and Lomiri, default to Wayland or Mir, respectively.</ins>

The default GUI stack is X11, which has been said to be rather insecure, even by the developers themselves, as it allows any program to view and manipulate input and output from any other program. While Wayland is said to be more secure, [keylogging through `/proc` is still possible](https://github.com/Aishou/wayland-keylogger).

<ins>Update: The mentioned exploit does not use `/proc`.</ins>

### But I always thought Linux was super safe and had almost no bugs?
The kernel alone contains million lines of code. [Syzbot, a bot used for testing Linux and other kernels finds plenty bugs every month, and many of them are not fixed](https://syzkaller.appspot.com/upstream).
### Hardened Linux distros
<ins>Update: As mentioned above, the Wayland keylogger relies on the `LD_PRELOAD` environment variable, meaning properly sandboxed applications may not be able to exploit it. However, the issues with the kernel (which does the majority of the sandboxing itself through the namespaces feature), as well as the applications themselves remain.</ins>

Actually hardened Linux distros like [Whonix](https://whonix.org) or [CLIP OS](https://clip-os.org/en/) do provide better security, however they come or will come with restrictions like restricted root or not being able to make files executable that were not installed by the package manager, or running any app outside of a restrictive sandbox, due to their use of restrictive MAC policies. I also consider it likely that none of them can fix the architectural issues with the kernel and GUI stacks, as well as the fact most Linux programs are not designed to be run in a sandbox.

## Android
Android is a Linux-based OS by Google that greatly improves on the security of the latter. All apps run in sandboxes confined 
from each other, it has strong SELinux policies, and several devices feature a full verified boot implementation, in the case of the 
Google Pixel line, it is especially good, as it is possible to use custom keys, as well as both unlocking and relocking the 
bootloader. Memory safe languages like Java and Kotlin are commonplace. It also partially fixes the issue with Linux being a 
monolithic kernel, as it often uses stub drivers in the kernel that actually rely on userspace drivers. Nevertheless, one 
should not overlook issues such as the way it has become a closed ecosystem, with many devices being non-repairable, the use 
of a a very specific, apparently hard to port API (as the fact the Anbox compatibility layer for Linux does not work that 
great shows), and the reliance of many apps on proprietary Google services (though, to be fair, this is less common in apps 
that are free and open source software). Another issue is that neither the apps nor the GUI are really desktop friendly; they 
remain focused on the use on small touchscreens. For security, GrapheneOS, which is based on Android, should be considered.

## ChromiumOS
ChromiumOS is another OS by Google, based on Linux. Like Android, it uses SELinux policies, and offers isolation of 
desktop apps from the main operating system. However, there is one glaring issue with this: The central elemenet of the system, 
prominently featured in the name, and perhaps the one program with the largest attack surface; the web browser, is only 
isolated by its own sandbox as well as said SELinux policies, and not by the stronger isolation that their own KVM-based 
emulator crosvm offers, which is written in Rust and can thus be considered memory safe. It is also one of the worst 
operating systems for privacy due to [the way how login is handled](https://www.chromium.org/chromium-os/chromiumos-design-docs/login).

## BSD
FreeBSD, OpenBSD, NetBSD, HardenedBSD and so on all promise secure operating systems. While they are probably not as bad as 
Linux, especially on servers, their security is still lacking, and they have no proper desktop security model at all, mainly 
because they use X11 for a GUI stack, and browsers that have sandboxes elsewhere are not sandboxed here. They still use 
memory unsafe languages and monolithic kernels. [Here](https://isopenbsdsecu.re/) is some more information on OpenBSD 
specifically, which, while having improved security for various programs, and utilizing quite a bit more hardening than Linux, 
is not actually as secure as it claims, due to aforementioned issues.
<ins> Update: NetBSD runs some drivers in user space by using a special "rump kernel", which should make them more portable. 
<a href="https://en.wikipedia.org/wiki/Netbsd#Virtualization">Source</a></ins>

## MacOS and iOS
<ins>Update: In the case of MacOS, it is actually very interesting, as it is one of the best sandboxes on desktop you can get… 
Furthermore, the GUI is said to be strong against keylogging. At the same time, iOS has a very strong sandbox and is good at
suspending background apps. A Hackintosh using [OpenCore](https://github.com/acidanthera/OpenCorePkg) allows you to install 
MacOS on non-Apple hardware.</ins>

These are [partially open source](https://opensource.apple.com) operating systems by Apple. They both have (locked-down) verified boot implementations, and 
both employ a certain degree of sandboxing for apps. There is not much to say, they are rather good, but the only devices where 
they properly run is Apple ones, which are known for rather bad value and low repairability. The security model, while okay, 
~~is not too interesting to be honest~~. Privacy-wise, they are fine if telemetry is disabled, especially considering the fact 
that both the Safari web browser and the sandbox try to minimize tracking that can be conducted by websites and apps, 
respectively.

## Windows
The main reasons I see this operating system being liked by the security community include the Hyper-V hypervisor, which 
[apparently is supposed to be formally verified](https://link.springer.com/chapter/10.1007/978-3-642-05089-3_51), as well as 
Windows Defender Application Guard (WDAG), which is supposed to run the Microsoft Edge browser and other applications in a 
specialized VM. Microsoft store apps are also sandboxed, however they are… well, mostly stereotypical apps, and less actual, 
common desktop software. Issues with this one include it is slow, cannot be properly used with a GUI (not that you would want 
to anyway, given how much the Windows command line sucks compared to the one on POSIX/UNIX systems), highly targeted, proprietary, 
costs a lot of money if obtained legally, especially if you want the better versions with more security features and such, and 
is a privacy nightmare out of the box, some of which cannot be disabled in the non-Enterprise/Education versions.

## Qubes OS
[Qubes OS](https://www.qubes-os.org) is actually one of my favorites, and it is what I used to write this post. It has a 
great desktop security model based on isolating different programs into different VMs, good hardware compatibility due to the 
use of a Linux kernel for drivers, and allows you to run instances of the same program isolated from each other (something 
that other systems mentioned here mostly cannot, at least not as easily). It also has mechanisms for secure file and 
clipboard sharing, and generally provides better desktop integration than one might expect from virtual machines. 
The way it is based on running VMs also means that you can use different operating systems inside a unified interface. Issues 
include that the Xen hypervisor is claimed to be less secure by some people at least (though this is partially mitigated by 
the Qubes team, and the possibility of switching to different ones in the future is there), and that it really is not easy to 
use, to the point where one could almost consider adding it to the next category. I will probably write a blog post soon 
explaining on how to set up Qubes OS, as there are quite some pitfalls that may lead to it not booting, especially when 
using disk encryption, despite the nice graphical installer… It is also quite resource intensive and 8GB of RAM might not be 
enough for heavy multitasking, speaking of experience. Nevertheless, if you can use it, it provides an exceptional desktop 
security model utilizing things like disposable VMs with or without network connection, as well as Whonix and the like.

## Experimental operating systems
These are operating systems that are noteworthy because of their security features, however are certainly not ready for daily 
use yet, and likely do not run on your hardware, unless, in some cases, you have an old (well, ancient) Lenovo ThinkPad.
### Plan 9 from Bell Labs
Plan 9 was an operating system created by Bell Labs, with the idea of making truly *everything* a file (unlike UNIX systems 
which have ioctls and all that stuff), and running each process in a confined namespace, making things like keyboard input only 
visible when the window was active, for example. The reason it was dismissed by many in favor of Linux and Windows might have 
been a [generation gap](https://fosdem.org/2020/schedule/event/generation_gaps/), which is really unfortunate, considering how 
interesting it was. It continues to live on as [the 9front fork](http://9front.org), which, while performing actual 
development, seems more focused on embedding memes into their documentation rather than actually improving it much or even 
just providing an HTTPS website… 
<ins> Update: 9front has an HTTPS website, but the certificate is invalid and issued for the wrong domain. Seriously, is Let's 
Encrypt <i>that</i> hard?</ins>

### Fuchsia
[Fuchsia](https://fuchsia.dev) is a new operating system by Google utilizing capability-based security, which is focused on 
security as well as privacy (though, given Google's history, a sane amount of skepticism should be applied to at least the 
latter claim, even if they made some improvements to Android in that direction). It uses the Zircon kernel, which is 
something in between a microkernel and a monolithic one, and has some kind of GUI… Memory safe languages are partially, though 
not fully, used. Due to the large company behind it, this one may have better chances at getting hold in the market and 
running on real hardware than others mentioned here.
### RedoxOS
[RedoxOS](https://redox-os.org) aims to implement an entire microkernel-based operating system based on the memory safe Rust 
programming language. However, a kernel cannot solely be written using memory safe code, meaning some portions of it remain 
unsafe, and apart from the traditional POSIX user and file permissions, it does not really seem to offer any meaningful 
security model on top of that.
### Genode
[Genode](https://genode.org) is not actually an operating system by itself, but rather a framework to implement 
capability-based security as a kind of low-level middleware, while using any kernel. It was originally developed at TU 
Dresden, but now has its own company, Genode Labs. They offer the prebuilt SculptOS based on the NOVA microkernel running 
Linux VMs for compatibility (note: not for security, as that is provided by the Genode stack), which may or may not run on 
your hardware (some drivers were ported from Linux). The general idea is to expose only the minimal amount of software and 
hardware needed for a program to run.
### seL4
[seL4](https://sel4.systems) is a formally verified, capability-based security microkernel that is used on embedded systems 
like SIM cards and such. The [Robigalia](https://robigalia.org) project wants to implement a userspace in Rust, however it 
appears to target IoT as well. Perhaps it can be used  as a kernel for Genode at some point, who knows. Some people have also 
considered it for Qubes because it can be used as a hypervisor.
<ins>Update: There has been a successful implementation of [Genode on seL4](https://genode.org/documentation/release-notes/16.08#Interactive_and_dynamic_workloads_on_top_of_the_seL4_kernel), 
and what can be seen as the default userland of seL4 is [CAmkES](https://docs.sel4.systems/projects/camkes/), bringing the 
total number of FOSS userlands for seL4 that are known to me to 3.</ins>

### Barrelfish
This one is probably similar to seL4 in some ways. It is developed at ETH Zurich, and not formally verified (yet). I cannot 
really tell what it is about, at time of writing [they do not even have an HTTPS website](http://www.barrelfish.org), so who 
knows whether it will actually be good for security, or really anything.
### SpectrumOS
[SpectrumOS](https://spectrum-os.org) essentially wants to be a NixOS-based Linux distro with crosvm from ChromiumOS ported 
to it, being in spirit similar to Qubes, while claiming to have "a lower barrier to entry and is easier to use and maintain 
than other such systems". There is not really anything that can be installed and tried out at this early stage of development.

## So, what should I use?
I guess you should have kind of decided on that by now. I have elaborated my opinion on most operating systems that are 
more or less relevant nowadays, and as I said, Qubes kind of is my favourite. It is not only used by me, but also by 
people like Edward Snowden and Micah Lee, as their website shows, as well as thousands of others… But it should have become 
clear that each has their own disadvantages, and you should consider how much you value each factor, definitely.

## Does it matter? Can't I just continue using Linux or whatever?
Of course you can. Due to the low market share, it is not even likely to get compromised, and you are likely not targeted… If 
you can mitigate the usual malspam, dodgy files and such, the only attack surface remaining really is your browser, which can 
be reduced by disabling JavaScript, or at least JIT compilation for JavaScript. Chromium is considered to be more secure than 
Firefox due to the stronger sandbox and site isolation (and Ungoogled Chromium can provide great privacy as well). But, if you 
want to stay on the safer side, consider using a different OS, or at least isolating different things into different VMs.
