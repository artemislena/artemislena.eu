---
title: A JSON Feed bot for Matrix
published_date: 2020-08-04 01:07:00 +0200
data: {author: "Pre-plurality (likely FantasyCookie17)"}
---

<ins>Update: The project moved from Gitlab to Codeberg; you will find it [here](https://codeberg.org/artemislena/jsonfeed-bot).</ins>

I wrote a Matrix bot based on [matrix-nio](https://github.com/poljar/matrix-nio) which parses and posts JSON feeds.

## Wait, what?

[JSON Feed](https://jsonfeed.org) is a JSON-based format which aims to replace, or at least offer an alternative to, the XML-based Atom and RSS formats. Now, I still offer an RSS feed, but since Cobalt also supports generating JSON Feed, and I find JSON to be a format that is more easy to read and write than XML, this is going to be the main, but of course not only feed for my blog. Now, Matrix already has an RSS integration, but that had some issues with my TLS1.3-only setup, which is why I decided to write this bot instead. [Look at the repo](https://gitlab.com/artemislena/jsonfeed-bot) for more technical details, as well as for how to use it yourself, and/or contribute to it.

## Even more stuff
After [tulir](https://matrix.to/#/@tulir:maunium.net) gave me some hints on how to improve my Python to work better with `asyncio` and such, due to mistakes being made in the code of the JSON Feed bot, I also incorporated these changes that I did to the aforementioned codebase in the code of the [Techlore FAQbot](https://gitlab.com/FantasyCookie17/techlore-faq-bot). The `datetime` in `time` should now be in the correct format, and, last but certainly not least, [I now have my own room for this blog and other stuff](https://matrix.to/#/#blog:artemislena.eu), which of course also has the JSON Feed bot, and is now the preferred method for contacting me and leaving comments.
