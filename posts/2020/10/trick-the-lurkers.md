---
title: Trick the lurkers!
published_date: 2020-10-13 21:30:38 +0200
data: {author: "Pre-plurality (likely FantasyCookie17)"}
---
<ins>Update: The project has been abandoned as it was quite ineffective, it seemed. The room does not exist anymore.</ins>

I think I have found a method to efficiently identify and act against [lurker bots](https://shivering-isles.com/odd-lurker-bots-on-matrix) on Matrix. If you are not aware what I am talking about, read the linked article - note: the "vigilant user" mentioned in there is me.

## So, what is this method?
I have found another sign of how to identify at least some of them - similarly to the [Voyager bot](https://github.com/turt2live/matrix-voyager-bot), they automatically join rooms where the aliases are mentioned, however, unlike that one, they do not place their read marker on messages containing aliases. This could for example be observed with [@bonvoyage:ungleich.ch](https://matrix.to/#/@bonvoyage:ungleich.ch), an MXID I am very certain of it being a lurker bot. This behaviour can be easily exploited: One just needs to periodically post an alias for a room that is known to not contain any lurkers, see whether anyone joins seemingly automatically (probably does not place their read marker, and probably looks like a lurker bot otherwise as well), and ban/kick any of those from your rooms. To make this even more effective, write a message that users should not join that room next to the alias.

## My plan
I have created a room, [#lurker-trap:fantasycookie17.onederfultech.com](https://matrix.to/#/#lurker-trap:fantasycookie17.onederfultech.com) to act as a centralized place for such a lurker bot trap. This has the advantage of enabling moderators, room admins and server admins to share information about their MXIDs, in order for the former two to ban proactively, and the latter to deactivate them. If you want to take part in this, you may join, however, immediately post a message that you would like to participate, in order to not look like a bot yourself, especially when some of the other signs fit for you. You will be promoted to PL2, which will allow you to promote others to that level, as well as kicking bots that have joined; of course you should only do so when posting the MXID to that room.

## Some considerations
* The default alias may be known to the lurker bot developers at some point, making them block their bots from joining it. For this reason, PL2s can also add aliases - adding different, unrelated aliases occasionally is appreciated.
* Just like the aliases, the "do not join this room message" may be recognized. Vary it from time to time.
* It is advised to redact the message shortly after posting it, in order to prevent cluttering your rooms with unnecessary scrollback.
* This does not prevent all kinds of lurker bots entirely. However, it does denounce those that try to efficiently spread across the Matrix network, which is already a huge advantage.
