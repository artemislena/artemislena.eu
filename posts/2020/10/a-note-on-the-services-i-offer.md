---
title: A note on the services I offer
published_date: 2020-10-08 23:10:15 +0200
data: {author: "Pre-plurality (likely FantasyCookie17)"}
---
Just a quick note - I offer some (currently just static) web services at the according link in the menu. Besides some notes - small snippets of information that do not really fit into a specific post - and a page for copypasta, you will also find some possibly more interesting things: A verify page for providing a second opinion on whether public keys and onion links are correct (let me know if I should add anything on there) and my latest addition, a sticker picker for Matrix based on [Tulir's Stickerpicker](https://github.com/maunium/stickerpicker/).
