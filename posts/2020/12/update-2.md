---
title: "Update II"
published_date: 2020-12-31 19:55:08 +0100
data: {author: "FantasyCookie17"}
---
This is likely going to be my last post this year, though maybe not my last post this week… Sounds strange, doesn't it? Anyway, I just wanted to inform you about some further changes and such.

## Personal computing hardware
I have mostly gotten used to my ZSA Moonlander keyboard, and I am writing this post using said keyboard, while it is connected to my MacBook Air, both of which I mentioned in the last update post. macOS is, in fact, rather nice, in my opinion, and [the Objective-See tools](https://objective-see.com) are a great addition.

## Server
I had to change my deploy script again, as there was no `minify` program available for macOS, instead, I use `css-html-js-minifier`, a Python program that handles minification, now. I ended up not actually migrating to Alpine Linux after I heard of [the poor security of its package manager](https://nitter.net/siosm/status/104075074304909312). So, I ended up staying on Void Linux for most of my servers, however, I improved SSH security and performance by changing both server and client auth ciphers to Ed25519 rather than the default ECDSA and RSA3072, respectively. I would also like to point out that my Nextcloud, Synapse and Murmur instances are now officially mentioned as a "friendcloud" on my services page, meaning that you can get an account on any of these if you ask me and I know you.
