---
title: "Do not use webapps in zero-trust designs"
published_date: 2020-12-13 13:37:59 +0100
data: {author: "FantasyCookie17"}
---
Seriously, do not. The point of zero trust designs, or minimal trust designs, for example, when end-to-end encryption (E2EE) is used, is to not need trust in any external middleman server, only in the code that is run on the endpoints, and in the users of the endpoints.

However, when webapps (or PWAs) are used, this is essentially broken. The server can just modify the JavaScript sent to the user, for example, to introduce tracking code. This can be done both en masse or for a single user. Now, you could possibly use a browser extension that alarms you when the code that was previously verified by an audit changes. However, even that might not be sufficient:

Let us take [ProtonMail](https://protonmail.com) as an example. ProtonMail likes to occasionally send out HTML mail to the users of the service containing the latest news concerning the service. What they could do is, the next time they send such an email, send a modified version containing a malicious script to all users whom they want to attack, while previously having sneakily introduced an XSS vulnerability that would not be found by an audit, either by some seemingly innocent changes in the code, or by a modified CSP or CORS HTTP header that is only sent to the users whom they want to attack. Once the attack is done, they have the decryption keys - now they just need to have the email modify itself back to the version sent to the other users, and not send any modified headers anymore. The attack would be innoticeable by any subsequent audits.

I am sure similar attacks would be possible with other services as well. Thus I conclude that if you do not want E2EE to be essentially broken, you should use actual apps downloaded more permanently on your client, preferably using reproducible builds or compiled yourself. (And yes, even Electron would be fine, or perhaps simply self-hosting the webapp on your own (local) server.)

<ins>Update: Of course this article is only on end-to-end encryption and similar. If such is not present, webapps do have one advantage ‒ the browser sandbox (at least of Chromium) is stronger than most application sandboxes used by operating systems.</ins>
