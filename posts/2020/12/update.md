---
title: "Update"
published_date: 2020-12-13 12:35:00 +0100
data: {author: "FantasyCookie17"}
---
Given the large amount of time that has passed since my last post, I thought I would just give you a small update on what happened recently. I have two more posts planned, and I hope I will be able to write them today…

## Changes to website and such
All my HTML and CSS now gets minified in the deploy script, just after the source archive is generated. Hopefully this will lead to less bandwidth usage and faster loading times, while at the same time providing better source code readability for those who download the source code. I also now made a separate `downloads` directory in `/services` for binary files/archives and such — those should neither get minified nor be included in the source code archive. Furthermore, I have wanted to switch my server VMs from Void Linux to Alpine Linux since quite a while now. I have not done so yet due to lack of time and motivation, however, I am planning to do so soon™ (to be fair, I might indeed do it during the Christmas holidays). As you may have noticed, the theme of this website was changed by me (with support by [uhoh](https://matrix.to/#/@uhoh:privacytools.io)), mainly due to complaints on accessibility and contrast, but overall, I actually like it more than the old one, and am still considering changing maybe one element a bit (hint: I am looking into `@keyframes` for the `<hgroup>`).

## Personal computing hardware
My [ZSA Moonlander](https://zsa.io/moonlander) arrived some time ago. It is a bit different than a traditional keyboard, and I am still in the process of getting used to it, but I think that in the end I will be able to work more efficiently and ergonomically than with my old keyboard. My MacBook Air M1 should also arrive soon…
### "A *MacBook*?! You traitor to the FOSS community!"
Not so fast. Yes, macOS has large closed source parts, but at least [*some* source code is available](https://opensource.apple.com). And, to be honest, I just got a bit tired of Linux. It is known to be rather insecure as a desktop OS, and those distros that try to improve things, as well as Qubes OS, where automatic file sync via Nextcloud is more or less impossible, do have other issues, regarding usability or otherwise. macOS, on the other hand, is not a completely disrespectful to privacy (I know, OCSP for developers certificates; just block `ocsp.apple.com` via `/etc/hosts`), and is reasonably secure thanks to some sandboxing and good hardware security, especially when using the M1 SoC (Secure Enclave, verified boot and so on).

So, anyway, after getting that out of the way, perhaps I will also be able to do some kind of a review of one or both of those, if I find anything important that is not covered elsewhere.
