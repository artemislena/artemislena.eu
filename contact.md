---
title: "Contact"
---
Here is how you can contact us:

<ul>

<li class="twitch" id="twitch">

## Twitch
We regularly stream games on [our Twitch account ArtemisLena](https://twitch.tv/artemislena). Feel free to follow, watch our streams, and chat with us!

</li>

<li class="signal" id="signal">

## Signal
[Signal](https://signal.org) is a well-known privacy-friendly messenger. Our shared account is [artemis.17](https://signal.me/#eu/ml-D8YMxBm6Wy0FghD1h7k9pPGhYjNidFirG6RJJ1RgCDIwoFSVE1KMLmtMjN-GU).

![A Signal QR code.](/static/signal.webp?h={{ site.data.hashes.static.signal-webp }})

</li>

<li class="matrix" id="matrix">

## Matrix
You can reach all of us on [Matrix](https://matrix.org), we have individual accounts there. Our MXIDs are listed on our Keyoxide profile, linked a few paragraphs below. If you are new, do *not* register an account on the matrix.org server, it's rather slow and has some other issues as well. Look at [joinmatrix.org's list](https://joinmatrix.org/servers/) instead. [We offer some web clients on our services page](/services/), including one that works on mobile (Cinny), so you don't even have to download an app. [We do have a public room for public comments and general chat](https://matrix.to/#/#artemis:artemislena.eu) which also contains a bot that notifies members of new posts on our blog. This blog does contain a few sexually or otherwise lewd-leaning posts (with appropriate content warnings), please confine comments on those to [our "lewd" room](https://matrix.to/#/#lewd:artemislena.eu).
</li>

<li class="email">

## E-mail
Please do not send us spam, including adverts for crypto currencies and sponsored posts. We do not offer paid services, and we do not want to buy your product. Sending us emails asking about any of that will get them reported as spam, especially if they show you didn't do any research on this website. Find our e-mail addresses on [Keyoxide](#keyoxide). If you don't want your email provider to be able to read the message, encrypt to our public PGP or [age](https://github.com/filosottile/age) key, [found on the Verify page](/services/verify.html#ours).
</li>

<li class="keyoxide" id="keyoxide">

## Keyoxide
We have [a Keyoxide profile](https://keyoxide.org/wkd/artemis%40artemislena.eu). This does not actually allow any direct messaging, but it can help with discovering various accounts and with establishing trust through the cryptographic signatures.
</li>

<li class="ntfy" id="ntfy">

## Ntfy
This does not require any app download or making an account, though there are apps available for Android and iOS, as well as a CLI client. If you want to send us a message anonymously, simply publish a notification to [the `admin-contact` topic](https://ntfy.artemislena.eu/admin-contact) on [ntfy.artemislena.eu](https://ntfy.artemislena.eu) (note this is one-way, we can't reply to you there, though you could leave us e.g. the phone number you're registered under on [Signal](#signal) to text you back), or [subscribe to our `announcements` topic](https://ntfy.artemislena.eu/announcements) to get notifications on important artemislena.eu things. [Read the documentation](https://ntfy.artemislena.eu/docs/) if you need help.
</li>

<li class="simplex">

## SimpleX Chat
[SimpleX Chat](https://simplex.chat) is a relatively new messenger that is supposed to be usable completely anonymously. You can contact us on there by clicking [this contact link](https://simplex.chat/contact#/?v=1-2&smp=smp%3A%2F%2FSkIkI6EPd2D63F4xFKfHk7I1UGZVNn6k1QWZ5rcyr6w%3D%40smp9.simplex.im%2FGxWfesGN9WZKbPD-Ot8e6k_uENxwzYkx%23%2F%3Fv%3D1-2%26dh%3DMCowBQYDK2VuAyEAh6Djkq0ngqHPr66oIoX80tWMArQEWwjWINal-n-9O1Y%253D%26srv%3Djssqzccmrcws6bhmn77vgmhfjmhwlyr3u7puw4erkyoosywgl67slqqd.onion) on a device that has the app installed, or by scanning the QR code below from within the app:

![A SimpleX QR code.](/static/simplex.webp?h={{ site.data.hashes.static.simplex-webp }})

<li class="mastodon" id="fedi">

## Fediverse
We all got accounts on [our own GoToSocial](https://social.artemislena.eu). Keep in mind DMs on the Fediverse are not end-to-end-encrypted (use [our age key](services/verify.html#our-age) if you want that). Our accounts are listed on [the Plurality page](/plurality.html) and [Keyoxide](#keyoxide).
</li>

<li class="telegram">

## Telegram
We do not endorse Telegram and mainly have it for certain public chats not available elsewhere, but if absolutely necessary, you can message [@artemislena](https://t.me/artemislena). "Secret chats" only, please (that is, end-to-end encrypted chats, so the Telegram servers cannot read messages you send us). Note: Telegram on desktop and in the browser apparently do not support "secret chats", so if you can, use a mobile app instead.
</li>

</ul>
