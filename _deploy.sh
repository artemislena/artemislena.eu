#!/bin/sh

# Deploy site to server

set -e
umask 022

# Calculate hashes to append as query parameters to the paths in /static
echo 'Calculating hashes…'
find static -type d -exec mkdir -p _data/hashes/{} \; >/dev/null
# This one replaces "." with "-" and prepends it if filename starts with a digit
find static -type f -not -name '.DS_Store' \
-exec sh -c 'echo "\"$(xxh32sum "$1" | cut -d " " -f 1 | xxd -r -p | base64 | sed s/=//g | tr +/ -_)\"" > "_data/hashes/$(dirname "$1")/$(basename "$1" | cut -c 1 | sed "s/[[:digit:]]/-/" | grep "\-")$(basename "$1" | tr . -).json"' sh {} \;

cobalt build

find _site -type f -name '*.css' -exec css-html-js-minify --quiet --overwrite {} \;
echo 'CSS minified.'
find _site -type f -name '*.html' -not -path '_site/services/kinklist.html' -not -path '_site/browse.html' -exec /opt/homebrew/bin/tidy -q -access 0 -wrap 0 -modify {} \;
echo 'HTML tidied.'

# Make the webring JSON available on the website
cp _data/webrings/beCrimeDoGay.json _site/services/downloads/

# Check if the files that are to be signed changed, if yes, sign them again, if not, copy the signatures over
if shasum -a512 -qc _minisign/to-be-signed.shasum; then
    cp _minisign/*.minisig _site/services/
else
    echo 'Signing files…'
    minisign -Sm _site/services/verify.html _site/services/canary.html
    cp _site/services/*.minisig _minisign/
    shasum -a512 _site/services/verify.html _site/services/canary.html > _minisign/to-be-signed.shasum
fi

echo 'Precompressing…'
find -E _site -type f -not '(' -regex '_site/(.*\.(png|zip|7z|jpeg|webp|br|gz|zst)|browse\.html|services/whoareyou\.html)' -or -size 1 ')' -exec ect -9 -gzip --mt-deflate {} \; -exec brotli -f {} \;

chmod -R a+rX _site

echo 'Authenticating to webserver…'
rsync -r --progress --del _site/ artemislena.eu:/srv/www/artemislena.eu/

rm -r _site/
echo 'Build files removed.'
