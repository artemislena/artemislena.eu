User-agent: *
Disallow: /services/downloads/
Disallow: /services/whoareyou.html

# Advertising
User-agent: Adsbot
User-agent: peer39_crawler
User-agent: peer39_crawler/1.0
User-agent: BLEXBot
Disallow: /
Allow: /ads.txt
Allow: /app-ads.txt

# IP-violation scanners
User-agent: TurnitinBot
User-agent: AcademicBotRTU
User-agent: CheckMarkNetwork/1.0 (+https://www.checkmarknetwork.com/spider.html)
User-agent: BrandVerity/1.0
User-agent: SemrushBot-FT

# Doxing bot
User-agent: PiplBot

# GenAI
User-agent: GPTBot
User-agent: Google-Extended
User-agent: Applebot-Extended
User-agent: ClaudeBot
User-agent: FaceBookBot
User-agent: Meta-ExternalAgent
User-agent: Cotoyogi
User-agent: Webzio-extended
User-agent: Kangaroo Bot
User-agent: GenAI
User-agent: SemrushBot-OCOB
User-agent: VelenPublicWebCrawler

Disallow: /

Sitemap: https://artemislena.eu/sitemap.xml
