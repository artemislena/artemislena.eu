---
title: Webrings
layout: webring.liquid
---
## Be crime do gay
Our site is a member of the Be crime do gay webring. Visit the other members' sites below, or [go to our homepage](/), also available through the menu. Want to join? Have a look at [the README](https://codeberg.org/artemislena/artemislena.eu). Your site needs to be a personal site, and you need to be queer in some way.
