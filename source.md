---
title: Source code
---

We have one Git repo on Codeberg of all the config, Liquid, Markdown and unminified CSS files that are used for building this site at [artemislena/artemislena.eu](https://codeberg.org/artemislena/artemislena.eu), and one with the config files for our servers at [artemislena/nixos-server-configs](https://codeberg.org/artemislena/nixos-server-configs).
