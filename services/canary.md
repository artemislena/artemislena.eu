---
title: "Warrant Canary"
layout: canary.liquid
published_date: 2025-01-11 16:07:10 +0100
---

We hereby declare that to our knowledge, at least up to the date specified above, the following statements are true:

* none of our actively used cryptographic keys and authentication credentials (barring those for accounts that are not important for the security of this server and our development accounts) are known to anyone but ourselves,
* we have not received any subpoenas or court orders forcing us to disclose any information that is not publically availabe,
* none of our servers and personal computers have been compromised by third parties,
* nobody has credibly impersonated any one of us,
* there has been no leak of private data in the process of being handled by any of our systems
* the information on the verify page is correct.

Known exceptions to these statements (reports on data leaks, attacks etc.):
* None so far.

Verify this page using [this Minisign signature](canary.html.minisig) to make sure it is valid. The public key should be: `RWQOIDycKDQhQys8rP1C4lRLMna0CRGIUDqtRvTLLVNRD/tfNfnJXN1p`

This canary should not be older than one month. If no validly signed update has been done for several days exceeding that period of time, this likely means that at least one of the claims above cannot be reasonably guaranteed anymore.
