---
title: Services
---

We offer various services, both static ones on this site and dynamic ones on this domain, and give you the links to them here. Some of those might require JavaScript, which will be the only exceptions for the "No JS" rule for this site.

If anything is broken, it might be that that is due to our configuration, rather than the backend software, so feel free to [contact us](/contact.html) with any errors you encounter. We have a monitoring page for our services set up [on GitHub Pages](https://upptime.artemislena.eu). Note that, in order to avoid it not working due to our own infrastructure being broken, it runs on GitHub's servers, not our own.

The onion links are for users of Tor Browser or other browsers connecting to the Tor network. If you don't know what that is, you probably can safely ignore the onion links.

We do not take donations for legal reasons, and the cost for running all this is also relatively low. If you want to support our causes, you could donate to [ILGA-Europe](https://ilga-europe.org/donations/) or [Tafel Deutschland e.V.](https://www.tafel.de/spenden), for example, or one of the funds recommended by [Giving What We Can](https://www.givingwhatwecan.org).

Services currently offered:
<ul>
<li class="trans"><a href="https://gtrr.artemislena.eu">GTRR</a>: The Global Transgender Resources Registry. (<a href="http://664bnfuk2m52sdyfq52kzvzbouneltoczw2vcxctiahwphsgnzs22uyd.onion">🧅 Onion link</a>)</li>
<li class="verify"><a href="verify.html">Verify</a>: a page acting as a third party for verifying onion (and other dark web) links, and public cryptographic keys</li>
<li class="canary"><a href="canary.html">Warrant Canary</a></li>
<li class="notes"><a href="notes.html">Notes</a>: A collection of notes that we want to keep around, and that can be useful for the wider public as well. Old and not very updated.</li>
<li class="eyes"><a href="whoareyou.html">Whoareyou</a>: Shows you information that is exposed to (but never permanently collected by) this webserver. Useful for debugging and such, or for whatever else it is you would normally use sites like <a href="https://whatismyip.net" rel="nofollow">whatismyip.net</a> for.</li>
<li class="pasta"><a href="copypasta.html">Copypasta</a>: A small collection of copypasta to, well, copy and paste. Mostly for archival purposes now, will probably not be updated anymore.</li>
<li class="cloud"><a href="https://send.artemislena.eu">Send</a>: Share files easily with end-to-end encryption. Just upload one or several files and get a link you can share with others.</li>
<li class="atom"><a href="https://element.artemislena.eu">Element</a>: Probably the most popular web client for <a href="https://matrix.org">Matrix</a>.</li>
<li class="red"><a href="https://red.artemislena.eu">Redlib</a>: A privacy-focused frontend for Reddit. (<a href="http://red.lpoaj7z2zkajuhgnlltpeqh3zyq7wk2iyeggqaduhgxhyajtdt2j7wad.onion">🧅 Onion link</a>)</li>
<li class="img"><a href="https://imgur.artemislena.eu">Rimgo</a>: A privacy-focused frontend for Imgur. (<a href="http://imgur.lpoaj7z2zkajuhgnlltpeqh3zyq7wk2iyeggqaduhgxhyajtdt2j7wad.onion">🧅 Onion link</a>)</li>
<li class="note"><a href="https://tok.artemislena.eu">ProxiTok</a>: A privacy-focused fronted for TikTok. (<a href="http://tok.lpoaj7z2zkajuhgnlltpeqh3zyq7wk2iyeggqaduhgxhyajtdt2j7wad.onion">🧅 Onion link</a>)</li>
<li class="fire"><a href="https://bw.artemislena.eu">BreezeWiki</a>: An ad-free, more privacy-friendly frontend for wikis hosted on Fandom. (<a href="http://bw.lpoaj7z2zkajuhgnlltpeqh3zyq7wk2iyeggqaduhgxhyajtdt2j7wad.onion">🧅 Onion link</a>)</li>
<li class="shield"><a href="https://pad.artemislena.eu">CryptPad</a>: A collection of different webapps for end-to-end encrypted collaborative document editing, form creation, whiteboard sharing and so on. Default storage quota is 4GB; <a href="/contact.html">contact us</a> if you need more.</li>
<li class="notif"><a href="https://ntfy.artemislena.eu">Ntfy</a>: A service for sending notifications and such. See <a href="/contact.html#ntfy">the contact page</a> for info on public topics. If you want to use this for your private topics, or as a <a href="https://unifiedpush.org">Unified Push</a> distributor, <a href="/contact.html">contact us</a> to get an account.</li>
<li class="genie"><a href="https://jitsi.artemislena.eu">Jitsi</a>: A webapp for video conferencing. Runs on our VPS, rather than at home.</li>
<li class="croc">Croc relay server: Croc is a command line tool for easy encrypted real-time file transfer, see <a href="https://github.com/schollz/croc">its GitHub repo</a> for more information. Use <code>--relay croc.artemislena.eu</code> as a command line flag for using our server. Runs on our VPS, rather than at home.</li>
<li class="play">Syncplay: Makes it possible to sync up different video players over the internet. Use <code>syncplay.artemislena.eu:8999</code> in your <a href="https://syncplay.pl/download/">Syncplay client</a> for using our server.</li>
<li class="friends">Friendcloud: We offer Matrix, BorgBackup, Invidious accounts, E-mail (with custom domains), and GoToSocial as services with closed registration; close friends and their friends can <a href="/contact.html">contact us</a> if they want an account.</li>
</ul>
