---
title: Verify!
---

This is a page acting as a third party source for additional verification of things like public key fingerprints or onion links. If the information you got on this should differ from what is on here, immediately [contact us](/contact.html)! This HTML file has a [detached Minisign signature](verify.html.minisig); i.e. run this to verify this page being signed by us (note the public key specified in the last command should match what you read elsewhere about our public Minisign key):
```sh
wget https://artemislena.eu/services/verify.html https://artemislena.eu/services/verify.html.minisig
minisign -Vm verify.html -P 'RWQOIDycKDQhQys8rP1C4lRLMna0CRGIUDqtRvTLLVNRD/tfNfnJXN1p'
```

<h2 id="ours">Our keys and links</h2>
<ul><li>

Onion link to this site: [lpoaj7z2zkajuhgnlltpeqh3zyq7wk2iyeggqaduhgxhyajtdt2j7wad.onion](http://lpoaj7z2zkajuhgnlltpeqh3zyq7wk2iyeggqaduhgxhyajtdt2j7wad.onion)
</li>
<li id="our-pgp">

PGP: `7973 D087 497E D49A F3DC D66E 412A 96FD 1D74 214B`. Available through the WKD or `keys.openpgp.org`. Commits prior to 2023-07-11 may be signed by `CE6696CE96B00747B91326724EDC4EF429A9FFF9`, available from `keys.openpgp.org`. Commits prior to 2021-07-15 may be signed by FantasyCookie17's `0E8ACFFAB70FA026DE5C2E958F70F211395DF2BB`, available for download on [this page](/services/downloads/fantasycookie17-old.asc).
</li>
<li id="our-minisign">

Minisign: `RWQOIDycKDQhQys8rP1C4lRLMna0CRGIUDqtRvTLLVNRD/tfNfnJXN1p`. You can verify messages signed by it with [this tool](https://github.com/jedisct1/minisign).
</li>
<li id="our-age">

age: `age1dcg4qvekdft3xpvd0sl6mcru59rfmvrsgw5qd3c98ye73pd0hpxskpwpx3`. You can encrypt messages with it using [this tool](https://github.com/filosottile/age).
</li></ul>

## Onion links
- Anna Aurora: [v53koizwicklp2os45yhts66dgpqivhzb3c4uggjvunhjfe4otrfdvid.onion](http://v53koizwicklp2os45yhts66dgpqivhzb3c4uggjvunhjfe4otrfdvid.onion)
- Privacy Guides: [eter4u55b667kuo72ntpm7ut54sa2mxmr22iqgzns4jw7boeox3qgyid.onion](http://eter4u55b667kuo72ntpm7ut54sa2mxmr22iqgzns4jw7boeox3qgyid.onion)
- Qubes OS: [qubesosfasa4zl44o4tws22di6kepyzfeqv3tg4e3ztknltfxqrymdad.onion](http://qubesosfasa4zl44o4tws22di6kepyzfeqv3tg4e3ztknltfxqrymdad.onion)
- Tor project: [2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53wid.onion](http://2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53wid.onion)

## PGP key fingerprints
- Anna Aurora: `E714 87CE 9FA3 0D35 ED96 68A6 B1BD DD56 BCE7 CD72`
- Aminda Suomalainen: `69FF 455A 869F 9031 A691 E0F1 9939 2F62 BAE3 0723`
- Christos Nouskas (Artix): `A574 A191 5CED E31A 3BFF 5A68 6065 20AC B886 B428`
- CinnyApp: `91AE 2D8C 1E27 5A69 9BFE BEFB 9433 0C7A 50AF CCF3`
- Eelco Dolstra (NixOS): `B541 D553 0127 0E0B CF15 CA5D 8170 B472 6D71 98DE`
- HulaHoop (Whonix): `04EF 2F66 6D36 C354 058B 9DD4 50C7 8B6F 9FF2 EC85`
- Qubes OS master: `427F 11FD 0FAA 4B08 0123 F01C DDFA 1A3E 3687 9494`
- Thomas Waldmann (BorgBackup security issue reporting): `6D5B EF9A DD20 7580 5747 B70F 9F88 FB52 FAF7 B393`
- Tor Browser Developers: `EF6E 286D DA85 EA2A 4BA7 DE68 4E2C 6E87 9329 8290`

## Signify/Minisign keys (people)
- Aminda: `RWQJBHYE4Q+4w7U4uvfOJHiqLYujzkyQpnWA2B3r29W3gFk4pw0+nwNX`
- DriftNotSkid/babba27: `RWSAnvfq8XXGcw5iUd2+q7OWwlITbIKkp5lUPKR3haFhdIWDdXFf1Rla`
- mazerfaker/ElongatedVeggie: `RWQtKYAZegCkBM6lRyq8jJdLqqN2k6mClhd6zc0wVnrp4TFjTc9K5X/d`
- Platitude: `RWTrLy2iopxN1RGn79L7vfPo7MYxn/nK5cVDXRzqLb4lYDSaW3GziC8C`
- Rocky: `RWBbQThyEcS2BHOZfghoIVM3DWiBGAagqY7iM+gu28GKqUHAvibL7A`
- SkyFox: `RWSxb5gftMn69bFHSOfM7flSRDB/eSjBpz97oXOZD0amGuybqyJ5JYL+`

## Signify/Minisign keys (organizations)
- GrapheneOS: `RWQZW9NItOuQYJ86EooQBxScfclrWiieJtAO9GpnfEjKbCO/3FriLGX3`
- OpenBSD 7.1: `RWR2eHwZTOEiTWog354iy3StRj18VbZl87O9uZpa1M2jGLXEkco6vDT5`

## SSH keys/fingerprints
- borg.artemislena.eu (for people using our Borg server): `ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOfydibZ7LEgmzwoV9aJx2vUfQvsaa4Gdsew/oiIGFPO`
- Codeberg: `ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIVIC02vnjFyL+I4RHfvIGNtOgJMe769VTF1VR4EB3ZB`
- GitHub: `ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOMqqnkVzrm0SdG6UOoqKLsabgH5C9okWi0dh2l9GKJl`
- GitLab: `ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf`
- Netcup's SFTP server: `SHA256:jyarNIyNjFXOlmpiYa9CqLjrINNO9Dln81vPXJn8wAU`

## age keys
- Anna Aurora: `age186mu24g0c8zra279gd44va43hacmwy94wdt69dvlhpp9y5lryddslk7ewh`
- mazerfaker/ElongatedVeggie: `age1t04fty25qjhs9nhyzf99rvxd9ymseed4xe6cpqxxhxfmjvx7ss9srum8x0`

## F-Droid repository fingerprints
- [Official F-Droid repo](https://f-droid.org/repo): `43 23 8D 51 2C 1E 5E B2 D6 56 9F 4A 3A FB F5 52 34 18 B8 2E 0A 3E D1 55 27 70 AB B9 A9 C9 CC AB`
- [FluffyChat](https://fluffychat.im/repo/stable/repo): `5E DB 5C 43 95 B2 F2 D9 BA 68 2F 6A 1D 27 51 70 CC E5 36 5A 6F A2 7D 22 20 EA 8D 52 A6 D9 5F 07`
- [FluffyChat nightly](https://fluffychat.im/repo/nightly/repo): `21 A4 69 65 73 00 57 64 78 B6 23 DF 99 D8 EB 88 9A 80 BC D9 39 AC A6 0A 40 74 74 1B EA EC 39 7D`
- [IzzyOnDroid](https://apt.izzysoft.de/fdroid/repo): `3B F0 D6 AB FE AE 2F 40 17 07 B6 D9 66 BE 74 3B F0 EE E4 9C 25 61 B9 BA 39 07 37 11 F6 28 93 7A`
- [Molly](https://molly.im/fdroid/repo): `3B 7E 93 B1 FE 32 C6 E3 5A 93 D6 DD FC 5A FB EB 12 39 A7 C6 EA 6A F2 0F F3 3E D5 3C DC 38 B0 4A`
- [Molly-FOSS](https://molly.im/fdroid/foss/fdroid/repo): `51 98 DA EF 37 FC 23 C1 4D 5E E3 23 05 B2 AF 45 78 7B D7 DF 20 34 DE 33 AD 30 2B DB 34 46 DF 74`
- [NewPipe](https://archive.newpipe.net/fdroid/repo): `E2 40 2C 78 F9 B9 7C 6C 89 E9 7D B9 14 A2 75 1F DA 1D 02 FE 20 39 CC 08 97 A4 62 BD B5 7E 75 01`
- [SpiritCroc's Apps](https://s2.spiritcroc.de/fdroid/repo): `66 12 AD E7 E9 31 74 A5 89 CF 5B A2 6E D3 AB 28 23 1A 78 96 40 54 6C 8F 30 37 5E F0 45 BC 92 42`

## Similar projects
- [AlwaysVerify](https://codeberg.org/ElongatedVeggie/AlwaysVerify), by ElongatedVeggie
