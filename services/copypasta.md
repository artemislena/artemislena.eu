---
title: Copypasta
---

This page contains some tech-related copypasta.

## [Richard Matthew Stallman](https://stallman.org)

### GNU/Linux
> I'd just like to interject for a moment. What you're refering to as Linux, is in fact, GNU/Linux, or as I've recently taken to calling it, GNU plus Linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX.
> 
> Many computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called Linux, and many of its users are not aware that it is basically the GNU system, developed by the GNU Project.
> 
> There really is a Linux, and these people are using it, but it is just a part of the system they use. Linux is the kernel: the program in the system that allocates the machine's resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or GNU/Linux. All the so-called Linux distributions are really distributions of GNU/Linux!

## [Jonah Aragon](https://aragon.sh)

### Tracker/ad blockers
> I'd just like to interject for a moment. What you're referring to as an ad-blocker, is in fact, an ad/tracker-blocker, or as I've recently taken to calling it, tracking protection. Adblocking does not provide tracking protection unto itself, but is rather another component of a fully functioning anti-tracking system made useful by vital components such as script and cookie blocking comprising a full tracking protection experience.
> 
> Many computer users run a these tracking protection systems every day, without realizing it. Through a peculiar turn of events, tracking protection suites which are widely used today are often called “adblockers”, and many of its users are not aware that it is blocking a plethora of online tracking scripts and data behind the scenes.
> 
> There really is a adblocker, and these people are using it, but it is just a part of the complete tracking protection system they use. Adblocking is the heart of the suite: the program in the system blocks the ads you see and interact with on many websites you visit. The adblocker is an essential part of a complete tracking protection system, but cannot provide adequate privacy protection by itself; it can only function in the context of a complete tracking protection system. Adblocking is normally used in combination with other tracking protection features: the whole system is basically tracking protection with ad blocking added, or ad/tracker-blocking. All the so-called “ad”blocking programs are really tracking protection suites.

### GrapheneOS
> To be fair, you have to have a very high IQ to understand GrapheneOS. The subject is extremely subtle, and without a solid grasp of the history of CopperheadOS most of the advice will go over a typical user's head. There's also the developer’s nihilistic outlook, which is deftly woven into their approach to the subject- their personal philosophy draws heavily from documented corporate fuckery, for instance. The true security gurus understand this stuff; they have the intellectual capacity to truly appreciate the depths of data security, to realise that this is not just important- they say something deep about SECURITY awareness. As a consequence people who dislike the work that goes into GrapheneOS truly ARE idiots- of course they wouldn't appreciate, for instance, the danger of big business' modus operandi as seen in the NSA's treatment of their existential boogeyman "Edward Snowden," who is himself a cryptic figure influenced by his personal belief in freedom. I'm smirking right now just imagining one of those addlepated simpletons scratching their heads in confusion as Google collects their entire life's worth of behavioral patterns. What fools.. how I pity them. 😂
> And yes, by the way, I DO have Micay’s face as a tattoo. And no, you cannot see it. It's for the ladies' eyes only- and even then they have to demonstrate that they're within 5 IQ points of my own (preferably lower) beforehand. Nothin personnel kid 😎

## [blippity bloopy](https://matrix.to/#/@blippitybloopy:privacytools.io)

### StartPage
> DuckDuckGo is red  
> StartPage is blue  
> If you think StartPage is bad  
> See [PrivacyTools Github issue 1562](https://github.com/privacytools/privacytools.io/issues/1562)

## [caves_](https://evertrue.neocities.org)

### NSA FUD
> DUUUUUUUUDE stop spreading FUD you're LITERALLY trying to damage the reputation of a BILLION DOLLAR CORPORATION that makes products that YOU DEPEND ON every day!!1 nobody knows that it's a backdoor anyways I mean what if it was for NSA internal usage dude!?!??/ besides why would the NSA put a backdoor in windows they don't even own the product OMEGALUL XD!!!! it's not like u have anyting to hide either!!!

## [Linus Sex Tips](https://matrix.to/#/@okbuddylinus:matrix.org)

### Brave
> What the fuck did you just fucking say about Brave, you little bitch? I’ll have you know I graduated top of my class in my online computer science degree, and I’ve been involved in numerous secret raids on the CopperheadOS room, and I have over 300 confirmed Signal contacts. I am trained in cyber warfare and I’m the top blogger in the entire Techlore - Main room. You are nothing to me but just another spreader of FUD. I will wipe you the fuck out with precision the likes of which has never been seen before on this Matrix room, mark my fucking words. You think you can get away with saying that shit to me over the Internet? Think again, fucker. As we speak I am contacting my secret network of cybersecurity bloggers across my homeserver and your IP is being traced right now so you better prepare for the storm, Moshiller. The storm that wipes out the pathetic little thing you call your OPSEC. You’re fucking dead, kid. I can be anywhere, anytime, and I can dox you in over seven hundred ways, and that’s just with Windows Command Prompt (Linux is incredibly insecure). Not only am I extensively trained in Chromium shilling, but I have access to the entire arsenal of Madaidan's Insecurities and I will use it to its full extent to wipe your miserable pro-Fennec argument off the face of the internet, you little shit. If only you could have known what unholy retribution your little “clever” comment was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn’t, you didn’t, and now you’re paying the price, you goddamn idiot. I will shit fury all over you and you will drown in it. You’re fucking pwned, kiddo.

## Tanith

<h3 id="teoq">Tanith's Education On Queers</h3>
> 1. All lesbians are U-haul lesbians. No exceptions.
> 2. All gay men are crossdressers. No exceptions. Apart from extreme bears.
> 3. All trans people get murdered by assholes.
> 4. All ace people wear black rings all over their hands.
> 5. All aro people are into BDSM.
> 6. Pan is like bi but with extra steps.
> 7. Genderfluid people need to make sure they drink enough. If not, they become agender.
> 8. Agender people are dehydrated genderfluid people.
> 9. All women are trans.
> 10. All men are trans.
> 11. There are no cis people.
> 12. Don’t blink when straight people are present.
> 13. You aren’t queer if you don’t have at least 5 labels.
> 14. Pan people are into kitchen tools.
> 15. If you’re not careful, you might fall in love with a gay person.
> 16. Breathing while cis people are present = instant death. Game over.
> 17. Straight people can’t bend their legs.

## [Yassie getting banned from T2](https://yasposting.mae.lgbt/blog/it-takes-t2-to-tango)
> This is against TOS. You've been reported. I get that you're excited to be here, but you gotta follow the rules. And this isn't it. You're not on Bluesky. We're building a kind and safe community here. This kind of post isn't the vibe we want.
