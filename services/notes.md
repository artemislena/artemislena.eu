---
title: Notes
---

A few notes to self that should be kept around and possibly shared with the public. 

<h2 id="gender_language">Gender-neutral/gender-inclusive ways of addressing groups of people (somehow some people get this wrong all the time)</h2>

* pals
* mates
* people
* y'all
* everyone
* everybody
* folks
* cats and kittens (I know, most people aren't either, but it still seems nice; I've seen this used by [Dinosaur Comics](https://qwantz.com) at least once, [here](https://qwantz.com/index.php?comic=1002))
* comrades
* creatures
* beings
* entities
* [surface-dwellers](https://qwantz.com/index.php?comic=3892) (Unless you… want to include… fish or marine mammals, I guess? Or flying creatures?)
* everynyan/everymeow
* everylings
* [buddies](https://qwantz.com/index.php?comic=3904) (apparently… Ryan North likes collecting gender-neutral words, too…)
* [sweethearts](https://qwantz.com/index.php?comic=4053) (maybe don't actually use this one)
 
## OSes and kernels
* [CLIP OS](https://clip-os.org)
* [Genode](https://genode.org)
* [Fuchsia](https://fuchsia.dev)
* [dahliaOS](https://dahliaos.io)
* [RedoxOS](https://redoxos.org)
* [SpectrumOS](https://spectrumos.org)
* [Muen](https://muen.sk)

## Hardware
* [Precursor](https://www.crowdsupply.com/sutajio-kosagi/precursor)
* [Raptor Computing Systems](https://raptorcs.com)

## CLI tools and utilities
* tldr
* thefuck
* fortune
* cowsay, lolcat
* [DEDA](https://github.com/dfd-tud/deda)
* [.zshrc](./downloads/zshrc)
* [Kakoune](https://kakoune.org)
* Pandoc ‒ try `--pdf-engine=wkhtmltopdf` (needs to be installed) if other things fail (especially if special characters are involved)
* mkcert or, even better, certstrap
* Paperkey (see [here](https://scribe.rip/@johnnymatthews/create-a-paper-backup-of-your-gpg-key-5e43894c59a) for a tutorial, note in the recovery step, `gpg` reading the piped output might not work, so you might have to put in a file and let GPG read that)

## Apps (both desktop and mobile)
* Transportr - app from F-Droid for public transport lookup.
* ElectronMail - desktop client for ProtonMail.
* Présentation - macOS app for making presentations out of PDFs.

## Book sources
* Gutenberg project
* Library Genesis

## System hardening
* [IsoAlloc](https://github.com/struct/isoalloc). On macOS, use [DYLIB injection](https://theevilbit.github.io/posts/dyld_insert_libraries_dylib_injection_in_macos_osx_deep_dive/)

## macOS
* Desktop sandbox
* Verified boot
* [Hackintosh](https://dortania.github.io/OpenCore-Install-Guide/)
* [Hosts list](https://github.com/nextdns/metadata/blob/master/privacy/native/apple)

## Networks/protocols
* IPFS
* Sphinx

## Cool websites (not necessarily privacy friendly)
* [gitignore.io](https://gitignore.io)

## Tutorials
* [WireGuard on OpenWrt (Turris)](https://doc.turris.cz/doc/en/public/wireguard)
* [Global gitignores](https://gist.github.com/subfuzion/db7f57fff2fb6998a16c)
* [NixOS on RockPro64](https://intj.com.br/rockpro64-nixos/) (basically follow wiki, but use U-Boot 2020.07 instead of latest (`nix-env -i /nix/store/k66wvh5h5vwdhx6n78nb50zpsn9ss2sg-uboot-rockpro64-rk3399_defconfig-2020.07`, will be available at that path then)
* [Upgrade Postgres on NixOS](https://nixos.org/manual/nixos/unstable/index.html#module-services-postgres-upgrading)
* [netcup API with cURL](https://forum.netcup.de/netcup-applications/ccp-customer-control-panel/15508-api-anfrage-mit-curl/), [netcup API docs](https://ccp.netcup.net/run/webservice/servers/endpoint.php)
* [Podman capabilities](https://fedoramagazine.org/podman-with-capabilities-on-fedora/)
* [Tor and I2P on NixOS (several parts)](https://mdleom.com/blog/2020/03/21/i2p-eepsite-nixos/)

## Baud rates
- RockPro64 w NixOS: `1500000` in bootloader, `9600` otherwise

## Interesting medication
- Agomelatine (antidepressant, serotonine antagonist and melatonine agonist, improves sleep quality without commonly causing dizziness, no sexual dysfunction reported as side effect)
- Lumateperone (antipsychotic, various serotonergic and dopaminergic activity, less side effects and possibly more effective at reducing negative symptoms than other antipsychotics)
